/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under 
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing author: Christina Payne (Vanderbilt U)
                        Stan Moore (Sandia) for dipole terms
------------------------------------------------------------------------- */
/* Fix Polar is used to fix polarization by imposed dipole moment method.
 * The command is used as :
 *          fix ID groupID polar cutoff tolerance maxiter polfile tholeflag(tholecoff) keyword value
 *             - ID, group ID are same as other fix
 *             - cutoff = Coulumb interaction cutoff
 *             - tolerance = calculation tolerance
 *             - maxiter = max iterations
 *             - polfile = file define polarizability
 *             - tholeflag = 0 no polarization catastrophe
 *                         = 1 linear method
 *                         = 2 exponential method
 *             -   if tholeflag != 0  thole's method coefficient
 *             - keyword = efield, mudamp, mustep, mumass
 *                - efield = ex, ey, ez
 *                     ex, ey, ez value = v_name
 *                               v_name = variable define the magnitude of electric field
 *                - SOR = SOR factor for Gauss Seidel Method
 * Preferred  Units: Efield = V/A
 *                   polarization = A^3
 *                   induceddipole = charge * A
 *---------------------------------------------------------------------------*/
#include "math.h"
#include "string.h"
#include "stdlib.h"
#include "fix_polar.h"
#include "atom.h"
#include "domain.h"
#include "comm.h"
#include "modify.h"
#include "neighbor.h"
#include "neigh_list.h"
#include "neigh_request.h"
#include "force.h"
#include "respa.h"
#include "input.h"
#include "update.h"
#include "variable.h"
#include "region.h"
#include "memory.h"
#include "error.h"
#include "group.h"
#include "pair.h"
using namespace LAMMPS_NS;
using namespace FixConst;
#define MAXLINE 1024

#define MAXDIA    1

enum{NONE,CONSTANT,EQUAL,ATOM};
enum{POLMU,FORCE};
/* ---------------------------------------------------------------------- */
// fix ID group-ID polar ex ey ez atom1 polarbility1 
FixPolar::FixPolar(LAMMPS *lmp, int narg, char **arg) :
  Fix(lmp, narg, arg)
{
  nullify();
  if (narg < 8) error->all(FLERR,"Illegal fix polar command");

  dynamic_group_allow = 1;
  scalar_flag = 1;
  global_freq = 1;
  extscalar = 1;

  force_flag = 0;

  cutoff = force->numeric(FLERR,arg[3]);
  tolerance = force->numeric(FLERR,arg[4]);
  maxiter = force->numeric(FLERR,arg[5]);
  read_file(arg[6]);


  tholeflag = force->numeric(FLERR,arg[7]);
  int nextarg = 8;
  switch (tholeflag) {
    case 0:
      thole = &FixPolar::thole_no;
      tholediff = &FixPolar::thole_no_diff;
      break;
    case 1:
      thole = &FixPolar::thole_linear;
      tholediff = &FixPolar::thole_linear_diff;
      tholecoff = force->numeric(FLERR,arg[nextarg]);
      nextarg++;
      break;
    case 2:
      thole = &FixPolar::thole_exp;
      tholediff = &FixPolar::thole_no_diff;
      tholecoff = force->numeric(FLERR,arg[nextarg]);
      nextarg++;
      break;
    default:
      error->all(FLERR, "Illegal fix polar command, wrong input for thole");
      break;
  }
  comm_forward = 3;
  exeflag = 0;
  SOR = 0.8;

  while ( nextarg < narg){
    if (strcmp(arg[nextarg],"efield") == 0){
      nextarg++;
      if (narg < nextarg+3) error->all(FLERR,"No enough input for fix_polar ex_efield");
      exeflag = 1;
      for (int i = 0; i < 3; i++){
        if (strstr(arg[nextarg],"v_") == arg[nextarg]){
          int len = strlen(&arg[nextarg][2])+1;
          efield_str[i] = new char[len];
          strcpy(efield_str[i],&arg[nextarg][2]);
        }
        else {
          efield[i] = force->numeric(FLERR,arg[nextarg]);
          estyle[i] = CONSTANT;
        }
        nextarg++;
      }
    }
    else if ( strcmp(arg[nextarg],"SOR") == 0){
      nextarg++;
      if (narg < nextarg+1) error->all(FLERR,"No enough input for fix_polar SOR");
      SOR = force->numeric(FLERR,arg[nextarg]);
      nextarg++;
    }
    else error->all(FLERR,"Illegal fix_polar command");
  }
}
/* ---------------------------------------------------------------------- */

FixPolar::~FixPolar()
{
  for (int i = 0; i < 3; i++) {
    delete [] efield_str[i];
  } 
  deallocate_storage();
  memory->destroy(alpha);
  memory->destroy(polflag);
}

/* ----------------------------------------------------------------------
   NULLIFY pointers 
------------------------------------------------------------------------- */

void FixPolar::nullify()
{
  sefield = NULL;
  apolflag = NULL;
  sefield_pol = NULL;
  polist = NULL;
  Min.A = NULL;
  Min.x = NULL;
  Min.b0 = NULL;
}
/* ----------------------------------------------------------------------
   Allocate variable 
------------------------------------------------------------------------- */
void FixPolar::allocate_init()
{
   int nlocal = atom->nlocal;
   int nall = atom->nlocal+atom->nghost;
   memory->create(sefield,nlocal,3,"polar:sefield");
   memory->create(apolflag,nall,"polar:apolflag");
}

/* ----------------------------------------------------------------------
   Allocate memory for polarization 
------------------------------------------------------------------------- */

void FixPolar::allocate_polar()
{
  int m = npol[0]*3;
  memory->create(sefield_pol,npol[2],npol[2],9,"polar:sefield_pol");
  memory->create(polist,npol[2],"polar:polist");
  memory->create(Min.A,m*m,"polar:Min.A");
  memory->create(Min.x,npol[2]*3,"polar:Min.x");
  memory->create(Min.b0,m,"polar:Min.b");
}

/* ---------------------------------------------------------------------- */

void FixPolar::deallocate_storage()
{
  memory->destroy(sefield);
  memory->destroy(apolflag);

  memory->destroy(sefield_pol);  
  memory->destroy(polist);
  memory->destroy(Min.x);
  memory->destroy(Min.A);
  memory->destroy(Min.b0);
}

/* ---------------------------------------------------------------------  */

void FixPolar::init_storage()
{
  int nlocal = atom->nlocal;
  int nall = atom->nlocal+atom->nghost;
  for (int i = 0; i < nlocal; i++) {
    for (int j = 0; j < 3; j++) {
      sefield[i][j] = 0.00;
    }
  }
  MPI_Barrier(world);
  for (int i = 0; i < npol[2]; i++) {
    for (int j = 0; j < npol[2]; j++) {
      for (int l = 0; l < 9; l++) {
        sefield_pol[i][j][l]=0.00;
      }
    }
  }
}

/* ----------------------------------------------------------------------- */

void FixPolar::reallocate_storage()
{
  deallocate_storage();
  allocate_init();
}

/* ---------------------------------------------------------------------- */

int FixPolar::setmask()
{
  int mask = 0;
  mask |= THERMO_ENERGY;
  mask |= POST_FORCE;
  mask |= POST_FORCE_RESPA;
  mask |= MIN_POST_FORCE;
  return mask;
}

/* ---------------------------------------------------------------------- */

void FixPolar::init()
{
  // check input variables
  if (exeflag == 1) {
    for (int i = 0; i < 3; i++) {
      char *ptr = efield_str[i];
      if (ptr) {
        efield_var[i] = input->variable->find(ptr);
        if (efield_var[i] < 0)
          error->all(FLERR,"External efield variable name for fix polar does not exist");
        if (input->variable->equalstyle(efield_var[i])) estyle[i] = EQUAL;
        else if (input->variable->atomstyle(efield_var[i])) estyle[i] = ATOM;
        else error->all(FLERR,"External efield variable for fix polar is invalid style");
      }
    }

    if (estyle[0] == ATOM || estyle[1] == ATOM || estyle[2] == ATOM)
      varflag = ATOM;
    else if (estyle[0] == EQUAL || estyle[1] == EQUAL || estyle[2] == EQUAL)
      varflag = EQUAL;
    else varflag = CONSTANT;
  }
  if (strstr(update->integrate_style,"respa"))
    nlevels_respa = ((Respa *) update->integrate)->nlevels;
  int irequest = neighbor->request(this,instance_me);
  neighbor->requests[irequest]->half = 0;
  neighbor->requests[irequest]->full = 1;
  neighbor->requests[irequest]->pair = 0;
  neighbor->requests[irequest]->fix  = 1;
}

/* ----------------------------------------------------------------------
   Initiate neigh_list 
------------------------------------------------------------------------- */

void FixPolar::init_list(int id, NeighList *ptr)
{
  list = ptr;
}

/* ---------------------------------------------------------------------- */

void FixPolar::setup(int vflag)
{
  if (strstr(update->integrate_style,"verlet"))
    post_force(vflag);
  else {
    ((Respa *) update->integrate)->copy_flevel_f(nlevels_respa-1);
    post_force_respa(vflag,nlevels_respa-1,0);
    ((Respa *) update->integrate)->copy_f_flevel(nlevels_respa-1);
  }
}

/* ---------------------------------------------------------------------- */

void FixPolar::min_setup(int vflag)
{
  post_force(vflag);
}


/* ----------------------------------------------------------------------
   Set apolflag and npol
------------------------------------------------------------------------- */

void FixPolar::set_apolflag()
{
  int nlocal = atom->nlocal;
  int nghost = atom->nghost;
  int *type = atom->type;
  int *mask = atom->mask;
  int m=0;

  npol[0] = 0;
  for (int i = 0; i < nlocal ; i++) {
      if ((polflag[type[i]] == 1) && (mask[i] & groupbit)){
        apolflag[i] = npol[0];
        npol[0]++;
      }
      else apolflag[i] = -1;
  }

  npol[1] = 0;
  for (int i = nlocal; i < nlocal+nghost ; i++){
      if ((polflag[type[i]] == 1) && (mask[i] & groupbit)){
        apolflag[i] = npol[0]+npol[1];
        npol[1]++;
      }
      else apolflag[i] = -1;
  }
  npol[2] = npol[0] + npol[1];
}

/* ----------------------------------------------------------------------
   Set polist 
------------------------------------------------------------------------- */

void FixPolar::set_polist()
{
  int nall = atom->nlocal + atom->nghost;

  int m = 0;
  for (int i = 0; i < nall; i++) {
    if (apolflag[i] != -1){
      polist[m] = i;
      m++;
    }
  }
}

/* ----------------------------------------------------------------------
   calculate E field induced by external field for local polarized  atoms
------------------------------------------------------------------------- */

void FixPolar::cal_sefield_ex()
{
  int nlocal = atom->nlocal;
  if (varflag == CONSTANT){
    for (int iatom = 0; iatom < nlocal; iatom++) 
      if (apolflag[iatom] != -1) 
        for (int j = 0; j < 3; j++) sefield[iatom][j] += efield[j];
  }
  else {
    modify->clearstep_compute();
    for (int j = 0; j < 3; j++) {
      if (estyle[j] == EQUAL){ 
        efield[j] = input->variable->compute_equal(efield_var[j]);
        for (int iatom = 0; iatom < nlocal; iatom++) 
          if (apolflag[iatom] != -1) 
            sefield[iatom][j] += efield[j];
      }
      else if (estyle[j] == ATOM)
        input->variable->compute_atom(efield_var[j],igroup,*sefield+j,3,1);
        //WARNING! Efields of non polarized atoms is also set here.
    }
    modify->addstep_compute(update->ntimestep+1);
  }
}

/* ----------------------------------------------------------------------
    calculate E field induced by permanent charge
------------------------------------------------------------------------- */

void FixPolar::cal_sefield_q()
{
  int inum = list->inum;
  int *ilist = list->ilist;
  int *numneigh = list->numneigh;
  int **firstneigh = list->firstneigh; 

  double **x = atom->x;
  double *q = atom->q;
  
  double  converter = force->qqrd2e/force->qe2f;   //Efield/converter = q/dis^2= mu/alpha
  for (int i = 0; i < inum; i++) {
    int iatom = ilist[i];
    if (apolflag[iatom] != -1){
      int *jlist = firstneigh[iatom];
      int jnum = numneigh[iatom];
      for (int j = 0; j < jnum; j++) {
        int jatom = jlist[j];
        jatom &= NEIGHMASK;

        double dx = x[iatom][0]-x[jatom][0];
        double dy = x[iatom][1]-x[jatom][1];
        double dz = x[iatom][2]-x[jatom][2];
        double dis;
        dis = sqrt(dx*dx+dy*dy+dz*dz);

        if (dis <= cutoff) {
          double dis_tri = pow(dis,3);
          double tholescreen[2];
          (this->*thole)(tholescreen, dis, iatom, jatom);
          sefield[iatom][0] += converter * dx * q[jatom] / dis_tri * tholescreen[0];
          sefield[iatom][1] += converter * dy * q[jatom] / dis_tri * tholescreen[0];
          sefield[iatom][2] += converter * dz * q[jatom] / dis_tri * tholescreen[0];
        }
      }
    }
  } 
}

/* ----------------------------------------------------------------------
   Compute sefield_pol factor Tij 
------------------------------------------------------------------------- */

void FixPolar::cal_sefield_fac_pol()
{
  int inum = list->inum;
  int *ilist = list->ilist;
  int *numneigh = list->numneigh;
  int **firstneigh = list->firstneigh;


  double converter = force->qqrd2e/force->qe2f;   //Efield/converter = q/dis^2= mu/alpha
  for (int i = 0; i < inum; i++) {
    int iatom = ilist[i];
    int idex = apolflag[iatom];
    if (idex != -1){
      
      // dipole dipole interaction
      int *jlist = firstneigh[iatom];
      int jnum = numneigh[iatom];
      for (int j = 0; j < jnum; j++) {
        int jatom = jlist[j];
        jatom &= NEIGHMASK;
        int jdex = apolflag[jatom];
        if (jdex != -1) {
          double T[3][3];
          compute_T(T,iatom,jatom);
          for (int ii = 0; ii < 3; ii++)
            for (int jj = 0; jj < 3;jj++){
              int m = ii*3+jj;
              sefield_pol[idex][jdex][m] = converter * T[ii][jj];
            }
        }
      }
      
      // dipole induced energy
      double a = alpha[atom->type[iatom]];
      sefield_pol[idex][idex][0] = converter / a;
      sefield_pol[idex][idex][4] = converter / a;
      sefield_pol[idex][idex][8] = converter / a;
    }
  }
}

/* ----------------------------------------------------------------------
   Compute matrix T 
   ------------------------------------------------------------------------- */

void FixPolar::compute_T(double (*T)[3], int iatom, int jatom)
{
  int *type = atom->type;
  double **x = atom->x;
  double dx[3],dis=0.0;

  for (int i = 0; i < 3; i++) {
    dx[i] = x[iatom][i] - x[jatom][i];
    dis += dx[i]*dx[i];
  }
  dis = sqrt(dis);
  double dis_tri = pow(dis,3);
  double dis_penta = pow(dis,5);

  double tholescreen[2];
  (this->*thole)(tholescreen, dis, iatom, jatom);
  tholescreen[0]  = tholescreen[0]/ dis_tri;
  tholescreen[1]  = 3*tholescreen[1]/dis_penta;

  for (int i = 0; i < 3; i++) {
    T[i][i] = tholescreen[0] - tholescreen[1]*dx[i]*dx[i];
  }
  T[0][1] = T[1][0] = -tholescreen[1]*dx[0]*dx[1];
  T[0][2] = T[2][0] = -tholescreen[1]*dx[0]*dx[2];
  T[1][2] = T[2][1] = -tholescreen[1]*dx[2]*dx[1];  
}

/* ----------------------------------------------------------------------
   Thole's method, linear
--------------------------------------------------------------------------- */

void FixPolar::thole_linear(double *f, double r, int iatom, int jatom)
{
  //r:   distance between two sites
  //i,j: atom ID
  //*f:  value for fe and ft
  double v = r;
  int  i,j;
  i = atom->type[iatom];
  j = atom->type[jatom];

  v /=  tholecoff * pow(alpha[i]*alpha[j],1.0/6.0);
  if ( v >= 1.0 ) {
    *f = 1.0;
    *(f+1) = 1.0;
  }else {
    *f = 4*pow(v,3) - 3*pow(v,4);
    *(f+1) = pow(v,4);
  }
}

void FixPolar::thole_linear_diff(double *f, double *r, double r0, int iatom, int jatom)
{
  double v = r0;
  int i = atom->type[iatom];
  int j = atom->type[jatom];
  v /= tholecoff * pow(alpha[i]*alpha[j],1.0/6.0); 
  if ( v >= 1.0 ) 
    f[0] = f[1] = f[2] = f[3] = f[4] = f[5] =0.0;
  else 
    for (int i = 0; i < 3; i++) {
      f[i] = 12.0*pow(v,3)*(1.0-v)*r[i]/(r0*r0);
      f[3+i] = 4.0*pow(v,4)*r[i]/(r0*r0);
    }
}
/* ----------------------------------------------------------------------
   Thole's method, exp 
------------------------------------------------------------------------- */

void FixPolar::thole_exp(double *f, double r, int iatom, int jatom)
{
  //r:   distance between two sites
  //iatom,jatom : atom ID
  //*f:  value for fe and ft
  double v = r;
  int  i,j;
  i = atom->type[iatom];
  j = atom->type[jatom];

  v /=  tholecoff * pow(alpha[i]*alpha[j],1.0/6.0);
  double w = pow(v,3);
  *f = 1.0-exp(-w);
  *(f+1) = 1.0 - (w+1)*exp(-w);
}

/* ----------------------------------------------------------------------
   Thole's method, no screening
------------------------------------------------------------------------- */

void FixPolar::thole_no(double *f, double r, int iatom, int jatom)
{
  *f = 1.0;
  *(f+1) = 1.0;
}

void FixPolar::thole_no_diff(double *f, double *r, double r0, int iatom, int jatom)
{
  for (int i = 0; i < 3; i++) {
    f[i] = 0.0;
    f[3+i] = 0.0;
  }
}

/* ----------------------------------------------------------------------
   Communication functions
------------------------------------------------------------------------- */

int FixPolar::pack_forward_comm(int n, int *list, double *buf, int pbc_flag, int *pbc)
{
  int m = 0;
  switch (packflag) {
    case POLMU:
      for (int i = 0; i < n; i++) {
        int iatom = list[i];
        int idex = apolflag[iatom];
        if ( idex != -1) {
          buf[m++] = Min.x[idex*3];
          buf[m++] = Min.x[idex*3+1];
          buf[m++] = Min.x[idex*3+2];
        }else {
          m += 3;
        }
      }
      break; 
    case FORCE:
      double **f = atom->f;
      for (int i = 0; i < n; i++) {
        int iatom = list[i];
        int idex = apolflag[iatom];
        if ( idex != -1) {
          buf[m++] = f[iatom][0];
          buf[m++] = f[iatom][1];
          buf[m++] = f[iatom][2];
        } else {
          m += 3;
        }
      }
      break;
  }
  return m;
}

void FixPolar::unpack_forward_comm(int n, int first, double *buf)
{
  int m = 0;
  int last = first+n;
  switch (packflag) {
    case POLMU:
      for (int i = first; i < last; i++) {
        int idex = apolflag[i];
        if (idex != -1) {
          Min.x[idex*3] = buf[m++];
          Min.x[idex*3+1] = buf[m++];
          Min.x[idex*3+2] = buf[m++];
        }else {
          m += 3;
        }
      }
      break;
    case FORCE:
      double **f = atom->f;
      for (int i = first; i < last; i++) {
        int idex = apolflag[i];
        if ( idex != -1) {
          f[i][0] = buf[m++];
          f[i][1] = buf[m++];
          f[i][2] = buf[m++];
        }else{
          m += 3;
        }
      }
      break;
  }
}
/* ----------------------------------------------------------------------
    Post Force 
------------------------------------------------------------------------- */

void FixPolar::post_force(int vflag)
{
  force_flag = 0;
  deallocate_storage();
  allocate_init();          // allocate sefield and apolflag
  set_apolflag();           // set apolflag and get npol
  allocate_polar();         // allocate sefield_pol and polist
  set_polist();             // set polist to map pol to atom
  init_storage();           // init sefield
  

  // calculate external efield if exist. 
  // CAUTION: NON POLARIZED ATOM ALSO CARRY EFIELD NOW IF EFIELD IS ATOM STYLE
  if ( exeflag == 1 ) cal_sefield_ex();

  // calculate permanent charge efield for POLARIZED ATOM
  cal_sefield_q();        
  // calculate dipole interaction factor
  cal_sefield_fac_pol();    //calculate sefield_pol
  // set matrix
  jacsolver_set();
  packflag = POLMU;
  comm->forward_comm_fix(this);  //Dist x for ghost atom
  int n = Min.size;   // n = npol[0]*3
  Msol.size = n;
  Msol.A = new double *[n];
  Msol.b0 = new double *[n];
  Msol.x = new double *[n];
  Msol.b = new double [n];

  int m = n*(n-1)/2;
  double *fac = new double[m]; 
  jacsolver_re(Min,Msol,fac);   //eliminate the lower triangular part of matrix A
  int iter = 0;
  /*
   *for (int i = 0; i < atom->nlocal+atom->nghost; i++) {
   *  if(atom->tag[i] == 673){
   *    char name[2];
   *    name[0] = comm->me + '0';
   *    name[1] = '\0';
   *    FILE *fp=fopen(name,"a+");
   *    int idex = apolflag[i];
   *    double imu[3];
   *    double **f = atom->f;
   *    imu[0] = Min.x[3*idex];
   *    imu[1] = Min.x[3*idex+1];
   *    imu[2] = Min.x[3*idex+2];
   *    if ( i <atom->nlocal) fprintf(fp,"This is local\n");
   *    fprintf(fp,"Timestep: %d before  Num: %d %d Index:%d\n",update->ntimestep,atom->nlocal,atom->nghost,i);
   *    fprintf(fp,"Force : %e,  %e,  %e\n",f[i][0],f[i][1],f[i][2]);
   *    fprintf(fp,"Dipole : %e,  %e,  %e\n",imu[0],imu[1],imu[2]);
   *    fprintf(fp,"------------------------------------------\n");
   *    fclose(fp);
   *  }
   *}
   */
  for ( iter = 0 ; iter < maxiter ; iter++) { 
    double res = 0.00;

    // set Msol.b
    for (int i = 0; i < npol[0]; i++) {
      for (int ii = 0; ii < 3; ii++) {
        int idex = i*3+ii;
        Msol.b[idex] = 0.00;
        for (int j = npol[0]; j < npol[2]; j++) {
          for (int jj = 0; jj < 3; jj++) {
            int jdex = j*3+jj;
            Msol.b[idex] -= sefield_pol[i][j][3*ii+jj]*(Min.x[jdex]);
          }
        }
      }
    }
    int ifac = 0;
    for (int i = 0; i < n-1; i++)
      for (int j = i+1; j < n; j++) {
        Msol.b[j] -= fac[ifac]*Msol.b[i];
        ifac++;
      }

    // iteration
    for (int i = 0; i < n; i++) {
      double xnew = *Msol.b0[i]+Msol.b[i];

      for (int j = i+1; j < n; j++)
        xnew -= (*(Msol.A[j]+i*n))*(*Msol.x[j]);
      xnew /= *(Msol.A[i]+i*n);
      xnew  = xnew*SOR + *Msol.x[i]*(1.0-SOR);  
      if ( fabs(xnew - *Msol.x[i]) > res)
        res=fabs(xnew - *Msol.x[i]);

      *Msol.x[i] = xnew;   //The lower triangular part of the matrix is zero
    }
    MPI_Allreduce(&res, &totres, 1, MPI_DOUBLE, MPI_MAX, world);

    packflag = POLMU;
    comm->forward_comm_fix(this);  //Dist x for ghost atom

    if ( totres < tolerance) break;
  }

  if (comm->me == 0) {
    if ( iter == maxiter) {
      char str[128];
      sprintf(str, "Induced dipole did not converge at step "BIGINT_FORMAT
          ":%lg",update->ntimestep,totres);
    }
  } 
  // Add force to atom->f
  sforce();
  //packflag = FORCE;
  //comm->forward_comm_fix(this);  //Dist x for ghost atom
  /*
   *for (int i = 0; i < atom->nlocal+atom->nghost; i++) {
   *  if(atom->tag[i] == 673){
   *    char name[2];
   *    name[0] = comm->me + '0';
   *    name[1] = '\0';
   *    FILE *fp=fopen(name,"a+");
   *    int idex = apolflag[i];
   *    double imu[3];
   *    double **f = atom->f;
   *    imu[0] = Min.x[3*idex];
   *    imu[1] = Min.x[3*idex+1];
   *    imu[2] = Min.x[3*idex+2];
   *    fprintf(fp,"Timestep: %d After  Num: %d %d  Index:%d\n",update->ntimestep,atom->nlocal,atom->nghost,i);
   *    fprintf(fp,"Force : %e,  %e,  %e\n",f[i][0],f[i][1],f[i][2]);
   *    fprintf(fp,"Dipole : %e,  %e,  %e\n",imu[0],imu[1],imu[2]);
   *    fprintf(fp,"------------------------------------------\n");
   *    fclose(fp);
   *  }
   *}
   */
  // release memory
  delete [] Msol.A;
  delete [] Msol.b0;
  delete [] Msol.x;
  delete [] Msol.b;
}

/* ---------------------------------------------------------------------- */

void FixPolar::post_force_respa(int vflag, int ilevel, int iloop)
{
  if (ilevel == nlevels_respa-1) post_force(vflag);
}

/* ---------------------------------------------------------------------- */

void FixPolar::min_post_force(int vflag)
{
  post_force(vflag);
}

/* ----------------------------------------------------------------------
   memory usage of local atom-based array
   NOT TRUE
------------------------------------------------------------------------- */

double FixPolar::memory_usage()
{
  double bytes = 0.0;
  if (varflag == ATOM) bytes = atom->nmax*4 * sizeof(double);
  return bytes;
} 

/* ----------------------------------------------------------------------
   Return added polarization energy
------------------------------------------------------------------------- */

double FixPolar::compute_scalar(void)
{
  if (force_flag == 0) {
    int inum = list->inum;
    int *ilist = list->ilist;
    int *numneigh = list->numneigh;
    int **firstneigh = list->firstneigh;

    double localenergy = 0.00;

    for (int i = 0; i < inum; i++) {
      int iatom = ilist[i];
      int idex = apolflag[iatom];
      if ( idex != -1 ) {
        double imu[3];
        imu[0] = Min.x[3*idex];
        imu[1] = Min.x[3*idex+1];
        imu[2] = Min.x[3*idex+2];

        // -p*E
        localenergy += -innerproduct(sefield[iatom],imu);

        int *jlist = firstneigh[iatom];
        int jnum = numneigh[iatom];
        for (int j = 0; j < jnum; j++) {
          int jatom = jlist[j];
          jatom &= NEIGHMASK;

          int jdex = apolflag[jatom];
          if (jdex != -1) {
            double jmu[3];
            jmu[0] = Min.x[3*jdex];
            jmu[1] = Min.x[3*jdex+1];
            jmu[2] = Min.x[3*jdex+2];
            
            double t1[3];
            for (int ii = 0; ii < 3; ii++)
              t1[ii] = innerproduct(sefield_pol[idex][jdex]+3*ii,jmu);

            localenergy += 0.5*innerproduct(imu,t1);
          } 
        }
      }
    }

    MPI_Allreduce(&localenergy,&totenergy,1,MPI_DOUBLE,MPI_SUM,world);
    force_flag = 1;
  }
  return totenergy;
}
/* ---------------------------------------------------------------------- 
 * read polarizability file
 ------------------------------------------------------------------------ */  
void FixPolar::read_file(char *file)
{
  int params_per_line = 2;
  char **words = new char*[params_per_line+1];

  int ntypes = atom->ntypes;

  memory->create(alpha,ntypes+1,"polar:alpha");
  memory->create(polflag,ntypes+1,"polar:polflag");
  for (int i = 0; i < ntypes+1; i++) {
    polflag[i] = 0;
    alpha[i] = 0.0;
  }

  // open file on proc 0

  FILE *fp;
  if (comm->me == 0) {
    fp = force->open_potential(file);
    if (fp == NULL) {
      char str[128];
      sprintf(str,"Cannot open fix polar parameter file %s",file);
      error->one(FLERR,str);
    }
  }

  // read each line out of file, skipping blank lines or leading '#'
  // store line of params if all 3 element tags are in element list

  int n,nwords,ielement,eof;
  char line[MAXLINE],*ptr;

  eof = ielement = 0;

  while (1) {
    if (comm->me == 0) {
      ptr = fgets(line,MAXLINE,fp);
      if (ptr == NULL) {
        eof = 1;
        fclose(fp);
      } else n = strlen(line) + 1;
    }
    MPI_Bcast(&eof,1,MPI_INT,0,world);
    if (eof) break;
    MPI_Bcast(&n,1,MPI_INT,0,world);
    MPI_Bcast(line,n,MPI_CHAR,0,world);

    ielement ++;
    if (ielement > ntypes)
      error->all(FLERR,"Invalid fix polar parameter file");

    // strip comment, skip line if blank

    if ((ptr = strchr(line,'#'))) *ptr = '\0';
    nwords = atom->count_words(line);
    if (nwords == 0) continue;

    // words = ptrs to all words in line

    nwords = 0;
    words[nwords++] = strtok(line," \t\n\r\f");
    while ((words[nwords++] = strtok(NULL," \t\n\r\f"))) continue;

    int itype = atoi(words[0]);
    alpha[itype]   = atof(words[1]);
    polflag[itype] = 1;
  }
  delete [] words;
}

/* ----------------------------------------------------------------------
Linear Equation solver by Jacobi Over Relaxation  method with pivoting and equilibrium 
   
   Credited to Han Luo

   Argument:  Ain = double *  pointer to matrix MUST BE 1-D CONTINUOUS DATA
              bin = double *  pointer to RHS
              n   = int       # of unknown
              xin = double *  pointer to initial guess and final result
   Implicit input : tolerance, SOR
   COMMENT: This is actually G-S method but due to parallel, 
             the ghost atoms limit the update of new value. 
------------------------------------------------------------------------- */  
//  Set matrix Min 
void FixPolar::jacsolver_set()
{
  Min.size = npol[0]*3;

  for (int i = 0; i < npol[0]; i++) {
    int iatom = polist[i];
    for (int ii = 0; ii < 3; ii++) {
      int idex = i*3+ii;    //row # of iatom in matrix A
      //Min.A
      int index = idex*Min.size;    // address of A[idex][0] in A's memory
      int index1 = 3*ii;            // address of T[ii][0] in memory
     
      for (int j = 0; j < npol[0]; j++)
        for (int jj = 0; jj < 3; jj++)
          Min.A[index+j*3+jj] = sefield_pol[i][j][index1+jj];
      
    //Min.b0 & Min.x
      Min.b0[idex]= sefield[iatom][ii];
      Min.x[idex] = sefield[iatom][ii]/sefield_pol[i][i][4*ii];
    }
  }
}
/*-----------------------------------------------------------------------*/
// Rearrange the matrix to eliminate lower triangular part 
// *fac is the facotr used to eliminated lower triangular part of A
void FixPolar::jacsolver_re(matrix_in Ain, matrix_sol Asol, double *fac)
{
  //point Asol to the first element of each column
  int n = Ain.size;
  for (int i = 0; i < n; i++) {
    Asol.A[i] = Ain.A+i;
    Asol.b0[i] = Ain.b0+i;
    Asol.x[i] = Ain.x+i;
  }
  if (MAXDIA){
    int ifac = 0;    //index for variable fac
    for (int i = 0; i < n-1; i++) {
      // exchange column to make diagonal dominant
      double largest = *(Asol.A[i]+i*n);
      int maxcol = i;
      for (int j = i+1; j < n ; j++) {
        if (fabs(*(Asol.A[j]+i*n)) > fabs(largest)) {
          largest = *(Asol.A[j]+i*n);
          maxcol = j;
        }
      }
      if ( maxcol != i){
        double *ptr = Asol.A[i];
        Asol.A[i] = Asol.A[maxcol];
        Asol.A[maxcol] = ptr;

        ptr = Asol.x[i];
        Asol.x[i] = Asol.x[maxcol];
        Asol.x[maxcol] = ptr;
      }

      // eliminate lower triangular part of the matrix
      for (int j = i+1; j < n; j++) {
        fac[ifac] = (*(Asol.A[i]+j*n)) / (*(Asol.A[i]+i*n));
        *(Asol.A[i]+j*n) = 0.0;
        *Asol.b0[j] -= *Asol.b0[i]*fac[ifac];
        for (int k = i+1; k < n; k++) {
          *(Asol.A[k]+j*n) -= *(Asol.A[k]+i*n)*fac[ifac];
        }
        ifac++;
      }
    }
  }
}

/* ----------------------------------------------------------------------
   Calculate the final force
------------------------------------------------------------------------- */
void FixPolar::sforce()
{
  int inum = list->inum;
  int *ilist = list->ilist;
  int *numneigh = list->numneigh;
  int **firstneigh = list->firstneigh; 
  double qqrd2e=force->qqrd2e;
  double **x = atom->x;
  double *q = atom->q;
  double **f = atom->f;

  for (int i = 0; i < inum; i++) {
    int iatom = ilist[i];
    int idex = apolflag[iatom];
    if ( idex != -1 ) {
      double imu[3];
      imu[0] = Min.x[3*idex];
      imu[1] = Min.x[3*idex+1];
      imu[2] = Min.x[3*idex+2];
      int *jlist = firstneigh[iatom];
      int jnum = numneigh[iatom];
      for (int j = 0; j < jnum; j++) {
        int jatom = jlist[j];
        jatom &= NEIGHMASK;
        double dx[3], dis=0.0;

        for (int ii = 0; ii < 3; ii++) { 
          dx[ii] = x[iatom][ii] - x[jatom][ii];
          dis += dx[ii]*dx[ii];
        }
        dis = sqrt(dis);    
        double dis_tri = pow(dis,3);
        double dis_quad = pow(dis,4);
        double dis_penta = pow(dis,5);

        double tdscreen[6];
        double tholescreen[2];
        (this->*tholediff)(tdscreen, dx, dis, iatom, jatom);
        (this->*thole)(tholescreen, dis, iatom, jatom);    //unit: 1/dis

        // p-q_0 interaction
        double t0 = innerproduct(imu,dx);
        if (dis <= cutoff)
          for (int ii = 0; ii < 3; ii++) {
            f[iatom][ii] += qqrd2e*imu[ii]*q[jatom]/dis_tri*tholescreen[0];
            f[iatom][ii] += -3.0*qqrd2e*q[jatom]*t0*dx[ii]/dis_penta*tholescreen[0];
            f[iatom][ii] += qqrd2e*q[jatom]*t0/dis_tri*tdscreen[ii];
          }

        //p-E interaction is not considered since the torque has no effect on point dipole

        int jdex = apolflag[jatom];
        // p-p interaction
        if (jdex != -1 && jdex > idex) {
          double jmu[3];
          jmu[0] = Min.x[3*jdex];
          jmu[1] = Min.x[3*jdex+1];
          jmu[2] = Min.x[3*jdex+2];

          // first part
          double t1 = innerproduct(jmu,dx);
          double t2 = innerproduct(imu,jmu);
          double fac = 3.0/dis_penta;
          for (int ii = 0; ii < 3; ii++) {
            double ftemp = 0.0;
            ftemp += qqrd2e*fac*t2*dx[ii]*tholescreen[0];
            ftemp -= qqrd2e/dis_tri*t2*tdscreen[ii]; 

            ftemp -= qqrd2e*fac*5.0/(dis*dis)*t0*t1*dx[ii]*tholescreen[1];
            ftemp += qqrd2e*fac*t1*imu[ii]*tholescreen[1];
            ftemp += qqrd2e*fac*t0*jmu[ii]*tholescreen[1];

            ftemp += qqrd2e*fac*t0*t1*tdscreen[3+ii];

            //iatom
            f[iatom][ii] += ftemp;

            //jatom
            f[jatom][ii] -= ftemp;
          }
        }
      }
    }
  }
}

/* ----------------------------------------------------------------------
   Inner product
------------------------------------------------------------------------- */
double FixPolar::innerproduct(double *a, double *b)
{
  double w = 0.0;
  for (int i = 0; i < 3; i++)
    w += (*(a+i))*(*(b+i));
  return w;
}
