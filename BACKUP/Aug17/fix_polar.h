/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under 
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifdef FIX_CLASS

FixStyle(polar,FixPolar)

#else

#ifndef LMP_FIX_POLAR_H
#define LMP_FIX_POLAR_H

#include "fix.h"

namespace LAMMPS_NS {

class FixPolar : public Fix {
 public:
  FixPolar(class LAMMPS *, int, char **);
  ~FixPolar();
  double compute_scalar();
  void init();
  void init_list(int, NeighList *);
  double memory_usage();
  void min_post_force(int);
  void min_setup(int);
  int pack_forward_comm(int, int *, double *, int, int *);
  void unpack_forward_comm(int, int, double *);
  void post_force(int);
  void post_force_respa(int, int, int);
  int setmask();
  void setup(int);

 private:
  int force_flag, varflag, exeflag; //exeflag:external efield
  class NeighList *list;
  int packflag;
  int vflag_post_force;
  // input parameter
  double cutoff;                        //Coulumb interaction cutoff
  double tolerance;                     //calculation tolerance
  int maxiter;                          //maxiteration 
  int *polflag;                         //polar flag for atomstyle
  double *alpha;                        //polarizability in Angstrom^3
  int tholeflag;                        //flag for thole's method
  double tholecoff;                     //thole's method coefficient
  double SOR;                           //SOR for matrix solver
  char *efield_str[3];                  //efield by variable
  double efield[3];                     //constant external efield
  int estyle[3],efield_var[3];          //efield style
  int nlevels_respa;
  double totres,totenergy;

  // polarization parameter
  double **sefield;                     //constant efield on polarized site
  double ***sefield_pol;                //sefield_pol(i,j)= T_{i,j} if i!=j, =1/alpha_i if i==j
  double npol[3];                       //npol[0] local atom [1] ghost atom [2] total # 
  int *apolflag;                        //polflag for atom, =1 if in the group and polarizable
  int *polist;

  typedef struct matrix_in {
    double *A;
    double *x;
    double *b0;
    int size;
  }matrix_in;

  matrix_in Min;


  typedef struct matrix_sol {
    double **A;
    double **x;
    double **b0;  //b0 = sefield
    double *b;    //b  = -ghost dipole * T
    int size;
  }matrix_sol;

  matrix_sol Msol;

  void read_file(char *);

  void (FixPolar::* thole)(double *, double, int, int);
  void thole_exp(double *, double, int, int);
  void thole_linear(double *, double, int, int);
  void thole_no(double *, double, int, int);

  void (FixPolar::* tholediff) (double *, double *, double, int, int);
  void thole_linear_diff(double *, double *, double, int, int);
  void thole_no_diff(double *, double *, double, int, int);

  void allocate_init();
  void allocate_polar();
  void deallocate_storage();
  void reallocate_storage();
  void init_storage();
  void nullify();

  void set_apolflag();
  void set_polist();

  void cal_sefield_ex();
  void cal_sefield_q();
  void cal_sefield_fac_pol();
  void compute_T(double (*)[3], int, int);

  void jacsolver_re(matrix_in, matrix_sol, double *);
  void jacsolver_set();
  void sforce();
  double innerproduct(double *, double *);
  double innerproduct(double *, double *, double);
  void addvec(double *, double *, double);

};

}

#endif
#endif

/* ERROR/WARNING messages:

E: Illegal ... command

Self-explanatory.  Check the input script syntax and compare to the
documentation for the command.  You can use -echo screen as a
command-line option when running LAMMPS to see the offending line.

E: Region ID for fix efield does not exist

Self-explanatory.

E: Fix efield requires atom attribute q or mu

The atom style defined does not have this attribute.

E: Variable name for fix efield does not exist

Self-explanatory.

E: Variable for fix efield is invalid style

The variable must be an equal- or atom-style variable.

E: Region ID for fix aveforce does not exist

Self-explanatory.

E: Fix efield with dipoles cannot use atom-style variables

This option is not supported.

W: The minimizer does not re-orient dipoles when using fix efield

This means that only the atom coordinates will be minimized,
not the orientation of the dipoles.

E: Cannot use variable energy with constant efield in fix efield

LAMMPS computes the energy itself when the E-field is constant.

E: Must use variable energy with fix efield

You must define an energy when performing a minimization with a
variable E-field.

*/
