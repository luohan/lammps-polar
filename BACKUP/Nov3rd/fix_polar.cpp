/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under 
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing author: Christina Payne (Vanderbilt U)
                        Stan Moore (Sandia) for dipole terms
------------------------------------------------------------------------- */
/* Fix Polar is used to fix polarization by imposed dipole moment method.
 * The command is used as :
 *          fix ID groupID polar cutoff tolerance maxiter polfile tholeflag(tholecoff) keyword value
 *             - ID, group ID are same as other fix
 *             - cutoff = Coulumb interaction cutoff
 *             - tolerance = calculation tolerance
 *             - maxiter = max iterations
 *             - polfile = file define polarizability
 *             - tholeflag = 0 no polarization catastrophe
 *                         = 1 linear method
 *                         = 2 exponential method
 *             -   if tholeflag != 0  thole's method coefficient
 *             - keyword = efield, SOR , noshift, TIP4P
 *                - efield = ex, ey, ez
 *                     ex, ey, ez value = v_name
 *                               v_name = variable define the magnitude of electric field
 *                - SOR = SOR factor for Gauss Seidel Method, default is 0.8
 *                - noshift = do not use damping function to shift the interaction
 *                - TIP4P = om, typeO, typeH, typeB, typeA
 *                      USE TIP4P model
 *                      om = the distance between M site and oxygen atom
 *                      typeO = type number of oxygen
 *                      typeH = type number of hydrogen
 *                         (note: similar to pair tip4p, this require atom ID of
 *                         oxygen and hydrogen to be consecutive)
 *                      typeB = type of O-H bond
 *                      typeA = type of H-O-H angle
 * Preferred  Units: Efield = V/A
 *                   polarization = A^3
 *                   induceddipole = charge * A
 *---------------------------------------------------------------------------*/
#include "math.h"
#include "string.h"
#include "stdlib.h"
#include "fix_polar.h"
#include "atom.h"
#include "angle.h"
#include "bond.h"
#include "domain.h"
#include "comm.h"
#include "modify.h"
#include "neighbor.h"
#include "neigh_list.h"
#include "neigh_request.h"
#include "force.h"
#include "respa.h"
#include "input.h"
#include "update.h"
#include "variable.h"
#include "region.h"
#include "memory.h"
#include "error.h"
#include "group.h"
#include "pair.h"
#include <iostream>
using namespace std;
using namespace LAMMPS_NS;
using namespace FixConst;
#define MAXLINE 1024

#define MAXDIA    1

enum{NONE,CONSTANT,EQUAL,ATOM};
enum{POLMU,PFORCE,VIRIAL,FORCE};     // flag for comm
/* ---------------------------------------------------------------------- */
// fix ID group-ID polar ex ey ez atom1 polarbility1 
FixPolar::FixPolar(LAMMPS *lmp, int narg, char **arg) :
  Fix(lmp, narg, arg)
{
  if (narg < 8) error->all(FLERR,"Illegal fix polar command");

  scalar_flag = 1;
  global_freq = 1;
  extscalar = 1;
  virial_flag = 1;
 
  force_flag = 0;
  shiftflag = 1;
  tip4pflag = 0;
  typeO = typeH = 0;
  debugflag = 0;
  nmax = 0;
  npol[0]=npol[1]=npol[2]=0;


  sefield = NULL;
  apolflag = NULL;
  sefield_pol = NULL;
  polist = NULL;
  Min.A = NULL;
  Min.x = NULL;
  Min.b0 = NULL;
  polflag = NULL;
  alpha = NULL;
  efield_str[0]=efield_str[1]=efield_str[2] = NULL;

  cutoff = force->numeric(FLERR,arg[3]);
  tolerance = force->numeric(FLERR,arg[4]);   
  maxiter = force->numeric(FLERR,arg[5]);
  read_file(arg[6]);

  tholeflag = force->numeric(FLERR,arg[7]);
  int nextarg = 8;
  switch (tholeflag) {
    case 0:
      thole = &FixPolar::thole_no;
      tholediff = &FixPolar::thole_no_diff;
      break;
    case 1:
      thole = &FixPolar::thole_linear;
      tholediff = &FixPolar::thole_linear_diff;
      tholecoff = force->numeric(FLERR,arg[nextarg]);
      nextarg++;
      break;
    case 2:
      thole = &FixPolar::thole_exp;
      tholediff = &FixPolar::thole_no_diff;
      tholecoff = force->numeric(FLERR,arg[nextarg]);
      nextarg++;
      break;
    default:
      error->all(FLERR, "Illegal fix polar command, wrong input for thole");
      break;
  }
  comm_forward = 3;
  comm_reverse = 4;
  exeflag = 0;
  SOR = 0.8;
  while ( nextarg < narg){
    if (strcmp(arg[nextarg],"efield") == 0){
      nextarg++;
      if (narg < nextarg+3) error->all(FLERR,"No enough input for fix_polar ex_efield");
      exeflag = 1;
      for (int i = 0; i < 3; i++){
        if (strstr(arg[nextarg],"v_") == arg[nextarg]){
          int len = strlen(&arg[nextarg][2])+1;
          efield_str[i] = new char[len];
          strcpy(efield_str[i],&arg[nextarg][2]);
        }
        else {
          efield[i] = force->numeric(FLERR,arg[nextarg]);
          estyle[i] = CONSTANT;
        }
        nextarg++;
      }
    }
    else if ( strcmp(arg[nextarg],"SOR") == 0){
      nextarg++;
      if (narg < nextarg+1) error->all(FLERR,"No enough input for fix_polar SOR");
      SOR = force->numeric(FLERR,arg[nextarg]);
      nextarg++;
    }
    else if ( strcmp(arg[nextarg],"noshift") == 0){
      shiftflag = 0;
      nextarg++;
    }
    else if ( strcmp(arg[nextarg],"TIP4P") == 0){
      tip4pflag = 1;
      om = force->numeric(FLERR,arg[++nextarg]);
      typeO = force->numeric(FLERR,arg[++nextarg]);
      typeH = force->numeric(FLERR,arg[++nextarg]);
      typeB = force->numeric(FLERR,arg[++nextarg]);
      typeA = force->numeric(FLERR,arg[++nextarg]);
      nextarg++;
    }   
    else if ( strcmp(arg[nextarg],"debug") == 0){
      debugflag = 1;
      nextarg++;
    }
    else error->all(FLERR,"Illegal fix_polar command");
  }
  nmax = atom->nmax;
  memory->create(sefield,nmax,3,"polar:sefield");
  memory->create(apolflag,nmax,"polar:apolflag");
}
/* ---------------------------------------------------------------------- */

FixPolar::~FixPolar()
{
  memory->destroy(efield_str[0]);
  memory->destroy(efield_str[1]);
  memory->destroy(efield_str[2]); 
  deallocate_polar();
  deallocate_init();
  memory->destroy(alpha);
  memory->destroy(polflag);
}

/* ----------------------------------------------------------------------
   Allocate variable 
------------------------------------------------------------------------- */
void FixPolar::allocate_init()
{
   memory->create(sefield,nmax,3,"polar:sefield");
   memory->create(apolflag,nmax,"polar:apolflag");
}

/* ----------------------------------------------------------------------
   Allocate memory for polarization 
------------------------------------------------------------------------- */

void FixPolar::allocate_polar()
{
  int m = npol[0]*3;
  memory->create(sefield_pol,npol[2],npol[2],9,"polar:sefield_pol");
  memory->create(polist,npol[2],"polar:polist");
  memory->create(Min.A,m*m,"polar:Min.A");
  memory->create(Min.x,npol[2]*3,"polar:Min.x");
  memory->create(Min.b0,m,"polar:Min.b");
}

/* ---------------------------------------------------------------------- */

void FixPolar::deallocate_init()
{
  memory->destroy(sefield);
  memory->destroy(apolflag);
}

void FixPolar::deallocate_polar()
{
  memory->destroy(sefield_pol);  
  memory->destroy(polist);
  memory->destroy(Min.x);
  memory->destroy(Min.A);
  memory->destroy(Min.b0);
}

/* ---------------------------------------------------------------------  */

void FixPolar::init_storage()
{
  int nlocal = atom->nlocal;
  int nall = atom->nlocal+atom->nghost;
  for (int i = 0; i < nlocal; i++) {
    for (int j = 0; j < 3; j++) {
      sefield[i][j] = 0.00;
    }
  }
  for (int i = 0; i < npol[2]; i++) {
    for (int j = 0; j < npol[2]; j++) {
      for (int l = 0; l < 9; l++) {
        sefield_pol[i][j][l]=0.00;
      }
    }
  }
}

/* ---------------------------------------------------------------------- */

int FixPolar::setmask()
{
  int mask = 0;
  mask |= THERMO_ENERGY;
  mask |= POST_FORCE;
  mask |= POST_FORCE_RESPA;
  mask |= MIN_POST_FORCE;
  return mask;
}

/* ---------------------------------------------------------------------- */

void FixPolar::init()
{
  // check input variables
  if (exeflag == 1) {
    for (int i = 0; i < 3; i++) {
      char *ptr = efield_str[i];
      if (ptr) {
        efield_var[i] = input->variable->find(ptr);
        if (efield_var[i] < 0)
          error->all(FLERR,"External efield variable name for fix polar does not exist");
        if (input->variable->equalstyle(efield_var[i])) estyle[i] = EQUAL;
        else if (input->variable->atomstyle(efield_var[i])) estyle[i] = ATOM;
        else error->all(FLERR,"External efield variable for fix polar is invalid style");
      }
    }

    if (estyle[0] == ATOM || estyle[1] == ATOM || estyle[2] == ATOM)
      varflag = ATOM;
    else if (estyle[0] == EQUAL || estyle[1] == EQUAL || estyle[2] == EQUAL)
      varflag = EQUAL;
    else varflag = CONSTANT;
  }
  if (strstr(update->integrate_style,"respa"))
    nlevels_respa = ((Respa *) update->integrate)->nlevels;
  int irequest = neighbor->request(this,instance_me);
  neighbor->requests[irequest]->half = 0;
  neighbor->requests[irequest]->full = 1;
  neighbor->requests[irequest]->pair = 0;
  neighbor->requests[irequest]->fix  = 1;
 
  // set parameter for TIP4P model
  if (tip4pflag == 1) {
    double theta = force->angle->equilibrium_angle(typeA);
    double blen = force->bond->equilibrium_distance(typeB);
    alpham = om / (cos(0.5*theta) * blen);
  }
}

/* ----------------------------------------------------------------------
   Initiate neigh_list 
------------------------------------------------------------------------- */

void FixPolar::init_list(int id, NeighList *ptr)
{
  list = ptr;
}

/* ---------------------------------------------------------------------- */

void FixPolar::setup(int vflag)
{
  if (strstr(update->integrate_style,"verlet"))
    post_force(vflag);
  else {
    ((Respa *) update->integrate)->copy_flevel_f(nlevels_respa-1);
    post_force_respa(vflag,nlevels_respa-1,0);
    ((Respa *) update->integrate)->copy_f_flevel(nlevels_respa-1);
  }
  neighbor->build_one(list);
}

/* ---------------------------------------------------------------------- */

void FixPolar::min_setup(int vflag)
{
  post_force(vflag);
}


/* ----------------------------------------------------------------------
   Set apolflag and npol
------------------------------------------------------------------------- */

void FixPolar::set_apolflag()
{
  int nlocal = atom->nlocal;
  int nghost = atom->nghost;
  int *type = atom->type;
  int *mask = atom->mask;

  npol[0] = 0;
  for (int i = 0; i < nlocal ; i++) {
      if ((polflag[type[i]] == 1) && (mask[i] & groupbit)){
        apolflag[i] = npol[0];
        npol[0]++;
      }
      else apolflag[i] = -1;
  }

  npol[1] = 0;
  for (int i = nlocal; i < nlocal+nghost ; i++){
      if ((polflag[type[i]] == 1) && (mask[i] & groupbit)){
        apolflag[i] = npol[0]+npol[1];
        npol[1]++;
      }
      else apolflag[i] = -1;
  }
  npol[2] = npol[0] + npol[1];
}

/* ----------------------------------------------------------------------
   Set polist 
------------------------------------------------------------------------- */

void FixPolar::set_polist()
{
  int nall = atom->nlocal + atom->nghost;

  int m = 0;
  for (int i = 0; i < nall; i++) {
    if (apolflag[i] != -1){
      polist[m] = i;
      m++;
    }
  }
}

/* ----------------------------------------------------------------------
   calculate E field induced by external field for local polarized  atoms
------------------------------------------------------------------------- */

void FixPolar::cal_sefield_ex()
{
  int nlocal = atom->nlocal;
  if (varflag == CONSTANT){
    for (int iatom = 0; iatom < nlocal; iatom++) 
      if (apolflag[iatom] != -1) 
        for (int j = 0; j < 3; j++) sefield[iatom][j] += efield[j];
  }
  else {
    modify->clearstep_compute();
    for (int j = 0; j < 3; j++) {
      if (estyle[j] == EQUAL){ 
        efield[j] = input->variable->compute_equal(efield_var[j]);
        for (int iatom = 0; iatom < nlocal; iatom++) 
          if (apolflag[iatom] != -1) 
            sefield[iatom][j] += efield[j];
      }
      else if (estyle[j] == ATOM)
        input->variable->compute_atom(efield_var[j],igroup,*sefield+j,3,1);
        //WARNING! Efields of non polarized atoms is also set here.
    }
    modify->addstep_compute(update->ntimestep+1);
  }
}

/* ----------------------------------------------------------------------
    calculate E field induced by permanent charge
------------------------------------------------------------------------- */

void FixPolar::cal_sefield_q()
{
  int inum = list->inum;
  int *ilist = list->ilist;
  int *numneigh = list->numneigh;
  int **firstneigh = list->firstneigh;
  double *special_coul = force->special_coul;
  int nlocal = atom->nlocal; 

  double **x = atom->x;
  double *q = atom->q;
  int *type = atom->type;

  double x1[3],x2[3];

  double  con = force->qqrd2e/force->qe2f;   //Efield/converter = q/dis^2= mu/alpha
  for (int i = 0; i < inum; i++) {
    int iatom = ilist[i];
    int idex = apolflag[iatom];
    int *jlist = firstneigh[iatom];
    int jnum = numneigh[iatom];

    for (int ii = 0; ii < 3; ii++) x1[ii] = x[iatom][ii];
    if (tip4pflag == 1 && type[iatom] == typeO)  adjust_oxygen(iatom,x1);

    for (int j = 0; j < jnum; j++) {
      int jatom = jlist[j];
      double factor_coul = special_coul[sbmask(jatom)];
      jatom &= NEIGHMASK;
      if (iatom < jatom) {
        int jdex = apolflag[jatom];

        if (idex != -1 || jdex != -1) {
          for (int jj = 0; jj < 3; jj++) x2[jj] = x[jatom][jj];
          // TIP4P model, assign M site coordinate to x2
          if (tip4pflag == 1 && type[jatom] == typeO) adjust_oxygen(jatom,x2);

          double dx = x1[0]-x2[0];
          double dy = x1[1]-x2[1];
          double dz = x1[2]-x2[2];
          double dis = sqrt(dx*dx+dy*dy+dz*dz);

          if (dis <= cutoff) {
            double dis_tri = pow(dis,3.0);

            // Thole's screen
            double tholescreen[2];
            (this->*thole)(tholescreen, dis, iatom, jatom);

            // Shift
            double s1 = 1.0;
            if (shiftflag == 1) {
              double eta = dis/cutoff;
              s1 = 1-3*pow(eta,2)+2*pow(eta,3);
            }

            double efactor = con * factor_coul / dis_tri * tholescreen[0] * s1;
            // jatom efield on iatom 
            if (idex != -1) {
              double efac = efactor * q[jatom];
              sefield[iatom][0] += efac * dx;
              sefield[iatom][1] += efac * dy;
              sefield[iatom][2] += efac * dz;
            }
            // iatom efield on jatom, if jatom is ghost atom, skip
            if (jdex != -1 && jatom < nlocal){
              double efac =-efactor * q[iatom];
              sefield[jatom][0] += efac * dx;
              sefield[jatom][1] += efac * dy;
              sefield[jatom][2] += efac * dz;
            }
          }
        }
      } 
    }
  }
}
  /* ----------------------------------------------------------------------
   Compute sefield_pol factor Tij 
------------------------------------------------------------------------- */

void FixPolar::cal_sefield_fac_pol()
{
  int inum = list->inum;
  int *ilist = list->ilist;
  int *numneigh = list->numneigh;
  int **firstneigh = list->firstneigh;
  double *special_coul = force->special_coul;
  double **x = atom->x;
  int *type = atom->type;
  double x1[3],x2[3];

  double con = force->qqrd2e/force->qe2f;   //Efield/converter = q/dis^2= mu/alpha
  for (int i = 0; i < inum; i++) {
    int iatom = ilist[i];
    int idex = apolflag[iatom];
    if (idex != -1){
      
      for (int ii = 0; ii < 3; ii++) x1[ii] = x[iatom][ii];
      if (tip4pflag == 1 && type[iatom] == typeO)  adjust_oxygen(iatom,x1);

      // dipole dipole interaction
      int *jlist = firstneigh[iatom];
      int jnum = numneigh[iatom];
      for (int j = 0; j < jnum; j++) {
        int jatom = jlist[j];
        jatom &= NEIGHMASK;
        int jdex = apolflag[jatom];

        if (jdex > idex ) {  //jdex >= 0
          for (int jj = 0; jj < 3; jj++) x2[jj] = x[jatom][jj];
          if (tip4pflag == 1 && type[jatom] == typeO) adjust_oxygen(jatom,x2);

          double dis = 0.0;
          double dx[3];
          for (int ii = 0; ii < 3; ii++) {
            dx[ii] =  x1[ii] - x2[ii];
            dis += dx[ii]*dx[ii];
          }
          dis = sqrt(dis);

          if ( dis <= cutoff ) {
            // Matrix T
            double T[3][3];
            compute_T(T,iatom,jatom,dis,dx);

            // Shift function for dipole-dipole
            double s2 = 1.0;
            if (shiftflag == 1) {
              double eta = dis/cutoff;
              s2 = 1-4.0*pow(eta,3.0)+3.0*pow(eta,4.0);
            }

            for (int ii = 0; ii < 3; ii++)
              for (int jj = 0; jj < 3;jj++){
                int m = ii*3+jj;
                sefield_pol[idex][jdex][m] = con * s2 * T[ii][jj];
                sefield_pol[jdex][idex][m] = con * s2 * T[ii][jj];
              }
          }
        }
      }

      // dipole induced energy
      double a = alpha[atom->type[iatom]];
      sefield_pol[idex][idex][0] = con / a;
      sefield_pol[idex][idex][4] = con / a;
      sefield_pol[idex][idex][8] = con / a;
    }
  }
}

/* ----------------------------------------------------------------------
   Compute matrix T 
   ------------------------------------------------------------------------- */

void FixPolar::compute_T(double T[][3], int iatom, int jatom, double dis, double dx[3])
{
  double dis_tri = pow(dis,3.0);
  double dis_penta = pow(dis,5.0);

  double tholescreen[2];
  (this->*thole)(tholescreen, dis, iatom, jatom);
  tholescreen[0]  = tholescreen[0]/ dis_tri;
  tholescreen[1]  = 3*tholescreen[1]/dis_penta;

  for (int i = 0; i < 3; i++) {
    T[i][i] = tholescreen[0] - tholescreen[1]*dx[i]*dx[i];
  }
  T[0][1] = T[1][0] = -tholescreen[1]*dx[0]*dx[1];
  T[0][2] = T[2][0] = -tholescreen[1]*dx[0]*dx[2];
  T[1][2] = T[2][1] = -tholescreen[1]*dx[2]*dx[1];  
}

/* ----------------------------------------------------------------------
   Thole's method, linear
--------------------------------------------------------------------------- */

void FixPolar::thole_linear(double *f, double r, int iatom, int jatom)
{
  //r:   distance between two sites
  //i,j: atom ID
  //*f:  value for fe and ft
  double v = r;
  int  i,j;
  i = atom->type[iatom];
  j = atom->type[jatom];

  v /=  tholecoff * pow(alpha[i]*alpha[j],1.0/6.0);
  if ( v >= 1.0 ) {
    *f = 1.0;
    *(f+1) = 1.0;
  }else {
    *f = 4*pow(v,3) - 3*pow(v,4);
    *(f+1) = pow(v,4);
  }
}

void FixPolar::thole_linear_diff(double *f, double *r, double r0, int iatom, int jatom)
{
  double v = r0;
  int i = atom->type[iatom];
  int j = atom->type[jatom];
  v /= tholecoff * pow(alpha[i]*alpha[j],1.0/6.0); 
  if ( v >= 1.0 ) 
    f[0] = f[1] = f[2] = f[3] = f[4] = f[5] =0.0;
  else 
    for (int i = 0; i < 3; i++) {
      f[i] = 12.0*pow(v,3)*(1.0-v)*r[i]/(r0*r0);
      f[3+i] = 4.0*pow(v,4)*r[i]/(r0*r0);
    }
}
/* ----------------------------------------------------------------------
   Thole's method, exp 
------------------------------------------------------------------------- */

void FixPolar::thole_exp(double *f, double r, int iatom, int jatom)
{
  //r:   distance between two sites
  //iatom,jatom : atom ID
  //*f:  value for fe and ft
  double v = r;
  int  i,j;
  i = atom->type[iatom];
  j = atom->type[jatom];

  v /=  tholecoff * pow(alpha[i]*alpha[j],1.0/6.0);
  double w = pow(v,3);
  *f = 1.0-exp(-w);
  *(f+1) = 1.0 - (w+1)*exp(-w);
}

/* ----------------------------------------------------------------------
   Thole's method, no screening
------------------------------------------------------------------------- */

void FixPolar::thole_no(double *f, double r, int iatom, int jatom)
{
  *f = 1.0;
  *(f+1) = 1.0;
}

void FixPolar::thole_no_diff(double *f, double *r, double r0, int iatom, int jatom)
{
  for (int i = 0; i < 3; i++) {
    f[i] = 0.0;
    f[3+i] = 0.0;
  }
}

/* ----------------------------------------------------------------------
   Communication functions
------------------------------------------------------------------------- */

int FixPolar::pack_forward_comm(int n, int *plist, double *buf, int pbc_flag, int *pbc)
{
  int m = 0,iatom;
  if (packflag == POLMU) {
    int idex;
    for (int i = 0; i < n; i++) {
      iatom = plist[i];
      idex = apolflag[iatom];
      if ( idex != -1) {
        buf[m++] = Min.x[idex*3];
        buf[m++] = Min.x[idex*3+1];
        buf[m++] = Min.x[idex*3+2];
      }
    }
  }else if (packflag == FORCE) {
    double **f=atom->f;
    for (int i = 0; i < n; i++) {
      iatom = plist[i];
      buf[m++] = f[iatom][0];
      buf[m++] = f[iatom][1];
      buf[m++] = f[iatom][2];
    }
  }
  return m;
}

void FixPolar::unpack_forward_comm(int n, int first, double *buf)
{
  int m = 0;
  int last = first+n;
  if (packflag == POLMU) {
    int idex;
    for (int i = first; i < last; i++) {
      idex = apolflag[i];
      if (idex != -1) {
        Min.x[idex*3] = buf[m++];
        Min.x[idex*3+1] = buf[m++];
        Min.x[idex*3+2] = buf[m++];
      }
    }
  }else if (packflag == FORCE) {
    double **f = atom->f;
    for (int i = first; i < last; i++) {
      f[i][0] = buf[m++];
      f[i][1] = buf[m++];
      f[i][2] = buf[m++];
    }
  }
}

int FixPolar::pack_reverse_comm(int n, int first, double *buf)
{
  int last = first+n;
  int m = 0;
  FILE * fid;
//  tagint *tag = atom->tag;
  if (debugflag) {
    if (update->ntimestep == 117 || update->ntimestep == 116) {
      char fname[15];
      sprintf(fname,"%d_for.dat",comm->me);
      fid = fopen(fname,"a+");
    }
  }
  if (packflag == PFORCE) {
    for (int i = first; i < last; i++) {
        buf[m++] = (double) comm->me;
        buf[m++] = pforce[i][0];
        buf[m++] = pforce[i][1];
        buf[m++] = pforce[i][2];
        if (debugflag) {
          if (update->ntimestep == 117 || update->ntimestep == 116) {
            if(buf[m-3] != 0.00 || buf[m-2] != 0.00 || buf[m-1] != 0.00) {
              int lflag;
              lflag = (i < atom->nlocal) ? 1 : 0;
              fprintf(fid, "Send Process: %d atom id: %d tag: %d local: %d \n",comm->me,i,atom->tag[i],lflag);
            }
            if (atom->tag[i] == 429){
              fprintf(fid,"Force of 429 atom = %f, %f, %f, id = %d, nlocal = %d\n",buf[m-3],buf[m-2],buf[m-1],i,atom->nlocal);
            }
          }
        }
    }
  }
  if (debugflag)
    if (update->ntimestep == 117 || update->ntimestep == 116)
    fclose(fid);
  return m;
}

void FixPolar::unpack_reverse_comm(int n, int *plist, double *buf)
{
  int m = 0,iatom;
  tagint ttag;
  FILE *fid;
  if (debugflag) {
    if (update->ntimestep == 117|| update->ntimestep == 116) {
    char fname[15];
    sprintf(fname,"%d_un.dat",comm->me);
    fid = fopen(fname,"a+");
   // fprintf(fid,"--------------------------\nTime step: %d\n",update->ntimestep);
    }
  }
  if (packflag ==  PFORCE) {
    double **f = atom->f;
    int recflag = 0;
    for (int i = 0; i < n; i++) {
      iatom = plist[i];
      if (update->ntimestep == 117 || update->ntimestep == 116) {
        if (recflag == 0) {
          fprintf(fid, "Recv from = %d id of 429 atom = %d \n",(int)(buf[m]+0.5),atom->map(429));
          recflag = 1;
        }
        if(buf[m+3] != 0.00 || buf[m+1] != 0.00 || buf[m+2] != 0.00) {
          int lflag;
          lflag = (i < atom->nlocal)?1:0;
          int ip = (int) (buf[m]+0.5);
          if (lflag)
            fprintf(fid,"Receive Process = %d, from = %d atom = %d, tag = %d\n ",comm->me,ip,iatom,atom->tag[iatom]);
        }
        if (atom->tag[iatom] == 429){
          int ip = (int) (buf[m]+0.5);
          fprintf(fid,"Force of 429 atom = %f, %f, %f from = %d, id = %d, nlocal = %d\n",buf[m+1],buf[m+2],buf[m+2],ip,iatom,atom->nlocal);
        }
      }
      m++;
      pforce[iatom][0] += buf[m++];
      pforce[iatom][1] += buf[m++];
      pforce[iatom][2] += buf[m++];
    }
  }
  if (debugflag)
    if (update->ntimestep == 117 || update->ntimestep == 116)
    fclose(fid);
}
/* ----------------------------------------------------------------------
    Post Force 
------------------------------------------------------------------------- */

void FixPolar::post_force(int vflag)
{
  int nlocal = atom->nlocal;
  int nall = atom->nlocal + atom->nghost;

  /*
   *if (debugflag == 1) {
   *  int it = 0;
   *  char hostname[256];
   *  gethostname(hostname,sizeof(hostname));
   *  printf("PID %d on %s ready for attach\n",getpid(),hostname);
   *  fflush(stdout);
   *  while (0 == it)
   *    sleep(5);
   *}
   */
  force_flag = 0;
  if (nall > nmax) {
    nmax = nall;
    deallocate_init();
    allocate_init();          // allocate sefield and apolflag
  }
  deallocate_polar();
  set_apolflag();           // set apolflag and get npol
  allocate_polar();         // allocate sefield_pol and polist
  set_polist();             // set polist to map pol to atom
  init_storage();           // init sefield
  if (vflag) v_setup(vflag);
  else     evflag = 0;
  // calculate external efield if exist. 
  // CAUTION: NON POLARIZED ATOM ALSO CARRY EFIELD NOW IF EFIELD IS ATOM STYLE
  if ( exeflag == 1 ) cal_sefield_ex();

  // calculate permanent charge efield for POLARIZED ATOM
  cal_sefield_q();        
  // calculate dipole interaction factor
  cal_sefield_fac_pol();    //calculate sefield_pol
  // set matrix
  jacsolver_set();
  packflag = POLMU;
  comm->forward_comm_fix(this);  //Dist x for ghost atom
  int n = Min.size;   // n = npol[0]*3
  Msol.size = n;
  Msol.A = new double *[n];
  Msol.b0 = new double *[n];
  Msol.x = new double *[n];
  Msol.b = new double [n];

  int m = n*(n-1)/2;
  double *fac = new double[m]; 
  jacsolver_re(fac);   //eliminate the lower triangular part of matrix A
  int iter = 0;
  totres = 0.00;

  for ( iter = 0 ; iter < maxiter ; iter++) { 
    double res = 0.00;

    // set Msol.b
    for (int i = 0; i < npol[0]; i++) {
      for (int ii = 0; ii < 3; ii++) {
        int idex = i*3+ii;
        Msol.b[idex] = 0.00;
        for (int j = npol[0]; j < npol[2]; j++) {
          for (int jj = 0; jj < 3; jj++) {
            int jdex = j*3+jj;
            Msol.b[idex] -= sefield_pol[i][j][3*ii+jj]*(Min.x[jdex]);
          }
        }
      }
    }
    if (MAXDIA) {
      int ifac = 0;
      for (int i = 0; i < n-1; i++)
        for (int j = i+1; j < n; j++) {
          Msol.b[j] -= fac[ifac]*Msol.b[i];
          ifac++;
        }
    }

    // iteration
    for (int i = 0; i < n; i++) {
      double xnew = *Msol.b0[i]+Msol.b[i];

      for (int j = i+1; j < n; j++)
        xnew -= (*(Msol.A[j]+i*n))*(*Msol.x[j]);
     
      xnew /= *(Msol.A[i]+i*n);
      xnew  = xnew*SOR + *Msol.x[i]*(1.0-SOR);  
      if ( fabs(xnew - *Msol.x[i]) > res)
        res=fabs(xnew - *Msol.x[i]);

      *Msol.x[i] = xnew;   //The lower triangular part of the matrix is zero
    }
    MPI_Allreduce(&res, &totres, 1, MPI_DOUBLE, MPI_MAX, world);
    
    packflag = POLMU;
    comm->forward_comm_fix(this);  //Dist x for ghost atom

    if ( totres < tolerance) break;
  }

  if (comm->me == 0) {
    if ( iter == maxiter) {
      char str[128];
      sprintf(str, "Induced dipole did not converge at step "BIGINT_FORMAT
          ":%lg",update->ntimestep,totres);
    }
  }

  //if (debugflag == 1)
   //check_solution(); 
  // Add force to atom->f
  sforce();
  delete [] Msol.A;
  delete [] Msol.b0;
  delete [] Msol.x;
  delete [] Msol.b;
}
/*--------------------------------------------------------------------------
 * Function to check matrix solution
 * --------------------------------------------*/
void FixPolar::check_solution()
{
  int *numneigh = list->numneigh;
  int **firstneigh = list->firstneigh; 
  double **res = new double *[npol[0]];
  int inum = list->inum;
  int *ilist = list->ilist;
  double imu[3];

  for ( int i = 0; i < npol[0]; i++)
    res[i] = new double [3];

  for (int i = 0; i < inum; i++) {
    int iatom = ilist[i];
    int idex = apolflag[iatom];
    if (idex != -1) {
      int *jlist = firstneigh[iatom];
      int jnum = numneigh[iatom];
      for (int ii = 0; ii < 3; ii++) 
        imu[ii] = Min.x[3*idex+ii];
      res[idex][0] = sefield[iatom][0];
      res[idex][1] = sefield[iatom][1];
      res[idex][2] = sefield[iatom][2];

      for (int j = 0; j < jnum; j++) {
        int jatom = jlist[j];
        jatom &= NEIGHMASK;
        int jdex = apolflag[jatom];
        if (jdex != -1) {
          double jmu[3];
          jmu[0] = Min.x[3*jdex];
          jmu[1] = Min.x[3*jdex+1];
          jmu[2] = Min.x[3*jdex+2];

          for (int ii = 0; ii < 3; ii++) {
            for (int jj = 0; jj < 3; jj++) {
              res[idex][ii] -= sefield_pol[idex][jdex][ii*3+jj]*jmu[jj];
            }
          }
        }
      }

      for (int ii = 0; ii < 3; ii++) 
        res[idex][ii] -= sefield_pol[idex][idex][4*ii]*imu[ii];
    }
  }
  double tol = 0.0,mtol;
  for (int idex = 0; idex < npol[0]; idex++) {
    for (int ii = 0; ii < 3; ii++) {
      if (res[idex][ii] > tol)
        tol=res[idex][ii];
    }
  }
  MPI_Allreduce(&tol,&mtol,1,MPI_DOUBLE,MPI_MAX,world);
  if (comm->me == 0)
    cout<<"Matrix solve = "<<mtol<<endl;

  for (int idex = 0; idex < npol[0]; idex++)
    delete [] res[idex];
  delete [] res;
}
/* ---------------------------------------------------------------------- */

void FixPolar::post_force_respa(int vflag, int ilevel, int iloop)
{
  if (ilevel == nlevels_respa-1) post_force(vflag);
}

/* ---------------------------------------------------------------------- */

void FixPolar::min_post_force(int vflag)
{
  post_force(vflag);
}

/* ----------------------------------------------------------------------
   memory usage of local atom-based array
   NOT TRUE
------------------------------------------------------------------------- */

double FixPolar::memory_usage()
{
  double bytes = 0.0;
  if (varflag == ATOM) bytes = atom->nmax*4 * sizeof(double);
  return bytes;
} 

/* ----------------------------------------------------------------------
   Return added polarization energy
------------------------------------------------------------------------- */

double FixPolar::compute_scalar(void)
{
  if (force_flag == 0) {
    int inum = list->inum;
    int *ilist = list->ilist;
    double localenergy = 0.00;
    for (int i = 0; i < inum; i++) {
      int iatom = ilist[i];
      int idex = apolflag[iatom];
      if ( idex != -1 ) {
        double imu[3];
        imu[0] = Min.x[3*idex];
        imu[1] = Min.x[3*idex+1];
        imu[2] = Min.x[3*idex+2];

        // -p*E
        localenergy += -0.5*force->qe2f*innerproduct(sefield[iatom],imu);
      }
    }
    MPI_Allreduce(&localenergy,&totenergy,1,MPI_DOUBLE,MPI_SUM,world);

    force_flag = 1;
  }

  return totenergy;
}
/* ---------------------------------------------------------------------- 
 * read polarizability file
 ------------------------------------------------------------------------ */  
void FixPolar::read_file(char *file)
{
  int params_per_line = 2;
  char **words = new char*[params_per_line+1];

  int ntypes = atom->ntypes;

  memory->create(alpha,ntypes+1,"polar:alpha");
  memory->create(polflag,ntypes+1,"polar:polflag");
  for (int i = 0; i < ntypes+1; i++) {
    polflag[i] = 0;
    alpha[i] = 0.0;
  }

  // open file on proc 0

  FILE *fp;
  if (comm->me == 0) {
    fp = force->open_potential(file);
    if (fp == NULL) {
      char str[128];
      sprintf(str,"Cannot open fix polar parameter file %s",file);
      error->one(FLERR,str);
    }
  }

  // read each line out of file, skipping blank lines or leading '#'
  // store line of params if all 3 element tags are in element list

  int n,nwords,ielement,eof;
  char line[MAXLINE],*ptr;

  eof = ielement = 0;

  while (1) {
    if (comm->me == 0) {
      ptr = fgets(line,MAXLINE,fp);
      if (ptr == NULL) {
        eof = 1;
        fclose(fp);
      } else n = strlen(line) + 1;
    }
    MPI_Bcast(&eof,1,MPI_INT,0,world);
    if (eof) break;
    MPI_Bcast(&n,1,MPI_INT,0,world);
    MPI_Bcast(line,n,MPI_CHAR,0,world);

    ielement ++;
    if (ielement > ntypes)
      error->all(FLERR,"Invalid fix polar parameter file");

    // strip comment, skip line if blank

    if ((ptr = strchr(line,'#'))) *ptr = '\0';
    nwords = atom->count_words(line);
    if (nwords == 0) continue;

    // words = ptrs to all words in line

    nwords = 0;
    words[nwords++] = strtok(line," \t\n\r\f");
    while ((words[nwords++] = strtok(NULL," \t\n\r\f"))) continue;

    int itype = atoi(words[0]);
    alpha[itype]   = atof(words[1]);
    polflag[itype] = 1;
  }
  delete [] words;
}

/* ----------------------------------------------------------------------
Linear Equation solver by Jacobi Over Relaxation  method with pivoting and equilibrium 
   
   Credited to Han Luo

   Argument:  Ain = double *  pointer to matrix MUST BE 1-D CONTINUOUS DATA
              bin = double *  pointer to RHS
              n   = int       # of unknown
              xin = double *  pointer to initial guess and final result
   Implicit input : tolerance, SOR
   COMMENT: This is actually G-S method but due to parallel, 
             the ghost atoms limit the update of new value. 
------------------------------------------------------------------------- */  
//  Set matrix Min 
void FixPolar::jacsolver_set()
{
  Min.size = npol[0]*3;

  for (int i = 0; i < npol[0]; i++) {
    int iatom = polist[i];
    for (int ii = 0; ii < 3; ii++) {
      int idex = i*3+ii;    //row # of iatom in matrix A
      //Min.A
      int index = idex*Min.size;    // address of A[idex][0] in A's memory
      int index1 = 3*ii;            // address of T[ii][0] in memory
     
      for (int j = 0; j < npol[0]; j++)
        for (int jj = 0; jj < 3; jj++)
          Min.A[index+j*3+jj] = sefield_pol[i][j][index1+jj];
      
    //Min.b0 & Min.x
      Min.b0[idex]= sefield[iatom][ii];
      Min.x[idex] = sefield[iatom][ii]/sefield_pol[i][i][4*ii];
    }
  }
}
/*-----------------------------------------------------------------------*/
// Rearrange the matrix to eliminate lower triangular part 
// *fac is the facotr used to eliminated lower triangular part of A
void FixPolar::jacsolver_re(double *fac)
{
  //point Msol to the first element of each column
  int n = Min.size;
  for (int i = 0; i < n; i++) {
    Msol.A[i] = Min.A+i;
    Msol.b0[i] = Min.b0+i;
    Msol.x[i] = Min.x+i;
  }
  if (MAXDIA){
    int ifac = 0;    //index for variable fac
    for (int i = 0; i < n-1; i++) {
      // exchange column to make diagonal dominant
      double largest = *(Msol.A[i]+i*n);
      int maxcol = i;
      for (int j = i+1; j < n ; j++) {
        if (fabs(*(Msol.A[j]+i*n)) > fabs(largest)) {
          largest = *(Msol.A[j]+i*n);
          maxcol = j;
        }
      }
      if ( maxcol != i){
        double *ptr = Msol.A[i];
        Msol.A[i] = Msol.A[maxcol];
        Msol.A[maxcol] = ptr;

        ptr = Msol.x[i];
        Msol.x[i] = Msol.x[maxcol];
        Msol.x[maxcol] = ptr;
      }

      // eliminate lower triangular part of the matrix
      for (int j = i+1; j < n; j++) {
        fac[ifac] = (*(Msol.A[i]+j*n)) / (*(Msol.A[i]+i*n));
        *Msol.b0[j] -= *Msol.b0[i]*fac[ifac];
        for (int k = i; k < n; k++) {
          *(Msol.A[k]+j*n) -= *(Msol.A[k]+i*n)*fac[ifac];
        }
        ifac++;
      }
    }
  }
}

/* ----------------------------------------------------------------------
   Calculate the final force
------------------------------------------------------------------------- */
void FixPolar::sforce()
{
  double imu[3],jmu[3];

  int *numneigh = list->numneigh;
  int **firstneigh = list->firstneigh; 
  double qqrd2e=force->qqrd2e;
  double **x = atom->x;
  double *q = atom->q;
  double **f = atom->f;
  tagint *tag = atom->tag;
  int *type = atom->type;

  int nlocal = atom->nlocal;
  int nghost = atom->nghost;
  int nall = atom->nghost+atom->nlocal;
  int inum = list->inum;
  int *ilist = list->ilist;
  double *special_coul = force->special_coul;

  double x1[3],x2[3];
 


  if (tip4pflag == 1) {
    memory->create(pforce,nall,3,"polar:pforce for tip4p H ghost");
    memory->create(ipforce,nall,"polar:pforce flag");
    for (int i = 0; i < nall; i++) {
     ipforce[i] = 0;
      pforce[i][0]=pforce[i][1]=pforce[i][2]=0.0;
    }
  }

  // variable for debugging
  double **added_force;
  double total_add_pro[3]={0.0,0.0,0.0},total_add[3]={0.0,0.0,0.0};
  if (debugflag == 1) {
    added_force = new double *[nlocal];
    for (int i = 0; i < nlocal; i++) {
      added_force[i] = new double[3];
      for (int j = 0; j < 3; j++) 
        added_force[i][j] = f[i][j];
    }
  }

  for (int i = 0; i < inum; i++) {
    int iatom = ilist[i];
    int idex = apolflag[iatom];
    int *jlist = firstneigh[iatom];
    int jnum = numneigh[iatom];
    if ( idex != -1) {
      for (int ii = 0; ii < 3; ii++) 
        imu[ii] = Min.x[3*idex+ii];
    }

    for (int ii = 0; ii < 3; ii++) x1[ii] = x[iatom][ii];
    if (tip4pflag == 1 && type[iatom] == typeO) adjust_oxygen(iatom,x1);

    for (int j = 0; j < jnum; j++) {
      int jatom = jlist[j];
      double factor_coul = special_coul[sbmask(jatom)];
      double con = qqrd2e * factor_coul;
      jatom &= NEIGHMASK;
      if (jatom > iatom) {
        int jdex = apolflag[jatom];

        if (idex != -1 || jdex != -1) {
          for (int jj = 0; jj < 3; jj++) x2[jj] = x[jatom][jj];
          if (tip4pflag == 1 && type[jatom] == typeO) adjust_oxygen(jatom,x2);
          // distance factor
          double dx[3],dis = 0.0;
          for (int ii = 0; ii < 3; ii++) {
            dx[ii] = x1[ii] - x2[ii];
            dis += dx[ii]*dx[ii];
          }
          dis = sqrt(dis);

          if ( dis <= cutoff ) {
            double dis_tri = pow(dis,3.0);
            double dis_quad = pow(dis,4.0);
            double dis_penta = pow(dis,5.0);

            if ( jdex != -1)
              for (int ii = 0; ii < 3; ii++) jmu[ii] = Min.x[3*jdex+ii];

            // Thole's screen
            double tdscreen[6], tholescreen[2];
            (this->*tholediff)(tdscreen, dx, dis, iatom, jatom);
            (this->*thole)(tholescreen, dis, iatom, jatom);

            // Shifting factors
            double s1 =1.0, s2 = 1.0;
            double s1d[3] = {0.0,0.0,0.0};
            double s2d[3] = {0.0,0.0,0.0};
            if (shiftflag == 1) {
              double eta = dis / cutoff;
              s1 = 1 - 3.0*pow(eta,2.0) + 2.0*pow(eta,3.0);
              s2 = 1 - 4.0*pow(eta,3.0) + 3.0*pow(eta,4.0);
              for (int ii = 0; ii < 3; ii++) {
                s1d[ii] = -6.0/pow(cutoff,2.0)*(1.0-dis/cutoff)*dx[ii];
                s2d[ii] = -12.0*dis/pow(cutoff,3.0)*(1.0-dis/cutoff)*dx[ii];
              }
            }

            //intiate force and some flag
            double fi[3] = {0.0,0.0,0.0};

            /* ----------------------------------------------------------------------
               imu - jq and iq - jmu interaction    
               ------------------------------------------------------------------------- */
            if (idex != -1 ) {
              double ft[3] = {0.0,0.0,0.0};
              double t0 = innerproduct(imu,dx);
              for (int ii = 0; ii < 3; ii++) {
                ft[ii] += con * imu[ii] * q[jatom] / dis_tri * tholescreen[0] * s1;
                ft[ii] +=-con * 3.0 * q[jatom] * t0 * dx[ii] /dis_penta * tholescreen[0] * s1;
                ft[ii] += con * q[jatom] * t0 / dis_tri * tdscreen[ii] * s1;
                ft[ii] += con * q[jatom] * t0 / dis_tri * tholescreen[0] * s1d[ii]; 
              }
              addvec(fi,ft,1.0);
            }
            if (jdex != -1 ) {
              double ft[3] = {0.0,0.0,0.0};
              double t0 = innerproduct(jmu,dx,-1.0);
              for (int ii = 0; ii < 3; ii++) {
                ft[ii] += con * jmu[ii] * q[iatom] / dis_tri * tholescreen[0] *s1;
                ft[ii] += con * 3.0 * q[iatom] * t0 * dx[ii] / dis_penta * tholescreen[0] *s1;  //minus for dx
                ft[ii] +=-con * q[iatom] * t0 / dis_tri * tdscreen[ii] * s1;  //minus for tdscreen
                ft[ii] +=-con * q[iatom] * t0 / dis_tri * tholescreen[0] * s1d[ii]; // minus for s1d
              }
              addvec(fi,ft,-1.0); 
            }
            /* ----------------------------------------------------------------------
               imu - jmu interaction 
               ------------------------------------------------------------------------- */
            if ( idex != -1 && jdex != -1){
              double ft[3] = {0.0,0.0,0.0};
              double t0 = innerproduct(imu,dx);
              double t1 = innerproduct(jmu,dx);
              double t2 = innerproduct(imu,jmu);
              double fac = 3.0/dis_penta;

              for (int ii = 0; ii < 3; ii++) {
                ft[ii] += qqrd2e * t2 * fac * dx[ii] * tholescreen[0] * s2;
                ft[ii] -= qqrd2e * t2 / dis_tri * tdscreen[ii] * s2; 

                ft[ii] -= qqrd2e * fac * 5.0 / (dis * dis) *t0 * t1 * dx[ii] * tholescreen[1] * s2;
                ft[ii] += qqrd2e * fac * t1 * imu[ii] * tholescreen[1] * s2;
                ft[ii] += qqrd2e * fac * t0 * jmu[ii] * tholescreen[1] * s2;
                ft[ii] += qqrd2e * fac * t0 * t1 * tdscreen[3+ii] * s2;

                ft[ii] += qqrd2e * (-t2 / dis_tri * tholescreen[0] + fac * t0 * t1 *tholescreen[1]) * s2d[ii];
              }
              addvec(fi,ft,1.0);
            }

            /* ----------------------------------------------------------------------
               Summation of force and  Virial calculation 
               iatom must be a local atom
               key[0] = 0 jatom is a ghost atom
               key[0] = 1 jatom is a local atom
               key[1] = 0 both atom are not TIP4P oxyegn
               key[1] = 1 iatom is TIP4P oxygen atom but jatom is not
               key[1] = 2 jatom is TIP4P oxygen atom but iatom is not
               key[1] = 3 both atom are TIP4P oxygen atom

               Force calculation:
               If the molecule is not TIP4P like molecule, calculate force pairwisely and add additional 
                      force directly to f.

               If it is TIP4P like molecule, for non Oxygen atom, force is directly add to the f. Otherwise
                      the force on O,H1 and H2 are calculated first then the force on H is added to pforce 
                      if H is a ghost atom. Then 
               ------------------------------------------------------------------------- */

            int nlist=0, vlist[6] , key[2]={0,0};   //flag for virial
            double v[6];
            //force on iatom
            if (tip4pflag == 1 && type[iatom] == typeO) {
              key[1] += 1;
              int iH1 = atom->map(tag[iatom]+1);
              int iH2 = atom->map(tag[iatom]+2);
              if (iH1 == -1 || iH2 == -1)
                error->one(FLERR,"TIP4P hydrogen is missing for polarization");
              if (type[iH1] != typeH || type[iH2] != typeH)
                error->one(FLERR,"TIP4P hydrogen has incorrect atom type for polarization");
              double fO[3]={0,0,0}, fH1[3]={0,0,0}, fH2[3]={0,0,0};
              addvec(fO,  fi, 1.0-alpham);
              addvec(fH1, fi, 0.5*alpham);
              addvec(fH2, fi, 0.5*alpham);
              addvec(f[iatom],fO,  1.0);
              
              if (iH1 < nlocal)
                addvec(f[iH1],  fH1, 1.0);
              else {
                addvec(pforce[iH1], fH1, 1.0);
                ipforce[iH1] = 1;
              }

              if (iH2 < nlocal)
                addvec(f[iH2],  fH2, 1.0);
              else { 
                addvec(pforce[iH2], fH2, 1.0);
                ipforce[iH1] = 1;
              }
              // iH1 and iH2 are possibly ghost atom, force needs to be communicated back
              vlist[nlist++] = iatom;
              vlist[nlist++] = iH1;
              vlist[nlist++] = iH2;
              if (evflag) {
                double xH1[3],xH2[3];
                domain->closest_image(x[iatom],x[iH1],xH1);
                domain->closest_image(x[iatom],x[iH2],xH2);

                v[0] = fO[0]*x[iatom][0] + fH1[0]*xH1[0] + fH2[0]*xH2[0];
                v[1] = fO[1]*x[iatom][1] + fH1[1]*xH1[1] + fH2[1]*xH2[1];
                v[2] = fO[2]*x[iatom][2] + fH1[2]*xH1[2] + fH2[2]*xH2[2];
                v[3] = fO[1]*x[iatom][0] + fH1[1]*xH1[0] + fH2[1]*xH2[0];
                v[4] = fO[2]*x[iatom][0] + fH1[2]*xH1[0] + fH2[2]*xH2[0];
                v[5] = fO[2]*x[iatom][1] + fH1[2]*xH1[1] + fH2[2]*xH2[1];
              }
            } 
            else {
              addvec(*(f+iatom),fi,1.0);
              vlist[nlist++] = iatom;
              if (evflag) {
                v[0] = fi[0]*x[iatom][0];
                v[1] = fi[1]*x[iatom][1];
                v[2] = fi[2]*x[iatom][2];
                v[3] = fi[1]*x[iatom][0];
                v[4] = fi[2]*x[iatom][0];
                v[5] = fi[2]*x[iatom][1];
              }
            }

            //force on jatom
            if (tip4pflag == 1 && type[jatom] == typeO) {
              key[1] += 2;
              int jH1 = atom->map(tag[jatom]+1);
              int jH2 = atom->map(tag[jatom]+2);
              if (jH1 == -1 || jH2 == -1)
                error->one(FLERR,"TIP4P hydrogen is missing for polarization");
              if (type[jH1] != typeH || type[jH2] != typeH)
                error->one(FLERR,"TIP4P hydrogen has incorrect atom type for polarization");
              double fO[3]={0,0,0}, fH1[3]={0,0,0}, fH2[3]={0,0,0};
              addvec(fO,  fi, -1.0+alpham);
              addvec(fH1, fi, -0.5*alpham);
              addvec(fH2, fi, -0.5*alpham);

              // if O is local atom, deal with added force
              // if O is not local atom, added force will be dealt by other process 
              if (jatom < nlocal) {
                key[0] = 1;
                addvec(f[jatom], fO,  1.0);

                // if H is local atom, add force to f
                // if H is ghost atom, add force to pforce for communication
                if (jH1 < nlocal)
                  addvec(f[jH1], fH1, 1.0);
                else {
                  addvec(pforce[jH1], fH1, 1.0);
                  ipforce[jH1] = 1;
                }

                if (jH2 < nlocal)
                  addvec(f[jH2], fH2, 1.0);
                else {
                  addvec(pforce[jH2], fH2, 1.0);
                  ipforce[jH2] = 1;
                }
              }
              
              vlist[nlist++] = jatom;
              vlist[nlist++] = jH1;
              vlist[nlist++] = jH2;
              if (evflag) {
                double xH1[3],xH2[3];
                domain->closest_image(x[jatom],x[jH1],xH1);
                domain->closest_image(x[jatom],x[jH2],xH2);

                v[0] += fO[0]*x[jatom][0] + fH1[0]*xH1[0] + fH2[0]*xH2[0];
                v[1] += fO[1]*x[jatom][1] + fH1[1]*xH1[1] + fH2[1]*xH2[1];
                v[2] += fO[2]*x[jatom][2] + fH1[2]*xH1[2] + fH2[2]*xH2[2];
                v[3] += fO[1]*x[jatom][0] + fH1[1]*xH1[0] + fH2[1]*xH2[0];
                v[4] += fO[2]*x[jatom][0] + fH1[2]*xH1[0] + fH2[2]*xH2[0];
                v[5] += fO[2]*x[jatom][1] + fH1[2]*xH1[1] + fH2[2]*xH2[1];
              }
            } 
            else {
              if (jatom < nlocal) {
                addvec(*(f+jatom),fi,-1.0);
                key[0] = 1;
              }
              vlist[nlist++] = jatom;
              if (evflag) {
                v[0] += -fi[0]*x[jatom][0];
                v[1] += -fi[1]*x[jatom][1];
                v[2] += -fi[2]*x[jatom][2];
                v[3] += -fi[1]*x[jatom][0];
                v[4] += -fi[2]*x[jatom][0];
                v[5] += -fi[2]*x[jatom][1];
              }
            }
            // tally virial 
            if (evflag) 
              v_tally_tip4p(key,vlist,v);
          }
        }
      }
    }
  }
  if (tip4pflag == 1) {
    packflag = PFORCE;
    if (update->ntimestep == 117 || update->ntimestep == 116) {
    char fname[15];
    sprintf(fname,"%d_for.dat",comm->me);
    FILE * fid = fopen(fname,"a+");
    fprintf(fid,"--------------------------\nTime step: %d Trigger\n",update->ntimestep);
    fclose(fid);
    }
    comm->reverse_comm_fix(this);
    for (int i = 0; i < nlocal; i++) {
      f[i][0] += pforce[i][0];
      f[i][1] += pforce[i][1];
      f[i][2] += pforce[i][2];
    }
  }
//  packflag = FORCE;
//  comm->forward_comm_fix(this);
  
  if (debugflag == 1){
    for (int i = 0; i < nlocal; i++) {
      for (int j = 0; j < 3; j++) {
        added_force[i][j] = f[i][j]-added_force[i][j];
        total_add_pro[j] += added_force[i][j];
      }
    }
    MPI_Allreduce(total_add_pro, total_add, 3, MPI_DOUBLE, MPI_SUM, world);
    MPI_Barrier(world);
    if (comm->me == 0)
      cout<<update->ntimestep<<"Total added_foce = "<<total_add[0]<<", "<<total_add[1]<<", "<<total_add[2]<<endl;
    MPI_Barrier(world);
    char fname[10];
    sprintf(fname,"%d.dat",comm->me);
    FILE * fid = fopen(fname,"a+");
    fprintf(fid,"--------------------------\nTime step: %d\n",update->ntimestep);
 //   fprintf(fid,"Total force for the process: %f %f %f\n",
   //     total_add_pro[0],total_add_pro[1],total_add_pro[2]);
  //  fprintf(fid,"Total force: %f, %f, %f \n",total_add[0],total_add[1],total_add[2]);
    for (int i = 0; i < nlocal; i++) {
      fprintf(fid,"atom = %d, type = %d, force = %f, %f, %f\n",atom->tag[i],atom->type[i],
          pforce[i][0],pforce[i][1],pforce[i][2]);
 //     if (atom->tag[i] == 429) {
  //      int hatom = i;
   //     cout<<"P"<<comm->me<<" ID= "<<i<<" FORCE ON 429 hydrogen: "
     //     <<f[hatom][0]<<", "<<f[hatom][1]<<", "<<f[hatom][2]<<endl;
    //  }
    }
    fclose(fid);
    for (int i = 0; i < nlocal; i++)
      delete [] added_force[i];
    delete [] added_force;
  }
  /*
   *char name[2];
   *name[0] = comm->me + '0';
   *name[1] = '\0';
   *FILE *fp=fopen(name,"a+");
   *fprintf(fp,"------------------------------------------\n");
   *for (int i = 0; i < atom->nlocal; i++) {
   *    int idex = apolflag[i];
   *    if (idex != -1) {
   *      double imu[3];
   *      imu[0] = Min.x[3*idex];
   *      imu[1] = Min.x[3*idex+1];
   *      imu[2] = Min.x[3*idex+2];
   *      fprintf(fp,"Iatom: %d Dipole : %e,  %e,  %e\n",i,imu[0],imu[1],imu[2]);
   *  }
   *}
   *fclose(fp);
   */
  if (tip4pflag == 1) {
    memory->destroy(pforce);
    memory->destroy(ipforce);
  }
}
/* ----------------------------------------------------------------------
   tally virial for TIP4P water, modified from pair function
   key is a array with size two
   iatom must be a local atom
   key[0] = 0 jatom is a ghost atom
   key[0] = 1 jatom is a local atom
   key[1] = 0 both atom are not TIP4P oxyegn
   key[1] = 1 iatom is TIP4P oxygen atom but jatom is not
   key[1] = 2 jatom is TIP4P oxygen atom but iatom is not
   key[1] = 3 both atom are TIP4P oxygen atom
   ------------------------------------------------------------------------- */

void FixPolar::v_tally_tip4p(int *key, int *vlist, double *v)
{
  if (vflag_global) {
    if (key[0])  
      //both atoms are local atoms
      for (int i = 0; i < 6; i++) {
        virial[i] += v[i];
      }
    else
      for (int i = 0; i < 6; i++) {
        virial[i] += 0.5*v[i];
      }
  }

  if (vflag_atom) {
    error->one(FLERR,"fix_polar doesn't support atom virial now");
/*
 *    int m = 0;
 *    if (key[1] == 0 || key[1] == 2)   //first one is not TIP4P oxygen
 *      for (int i = 0; i < 6; i++) 
 *        vatom[vlist[m++]][i] += 0.5*v[i];
 *    else
 *      for (int i = 0; i < 6; i++) {
 *        vatom[vlist[m++]][i] += 0.5*v[i]*(1.0-alpham);
 *        vatom[vlist[m++]][i] += 0.25*v[i]*alpham;
 *        vatom[vlist[m++]][i] += 0.25*v[i]*alpham;
 *      }
 *
 *    if (key[0] == 1) {
 *      if (key[1] == 0 || key[1] == 1) //second are not TIP4P oxygen
 *        for (int i = 0; i < 6; i++) 
 *          vatom[vlist[m++]][i] += 0.5*v[i];
 *      else 
 *        for (int i = 0; i < 6; i++) {
 *          vatom[vlist[m++]][i] += 0.5*v[i]*(1.0-alpham);
 *          vatom[vlist[m++]][i] += 0.25*v[i]*alpham;
 *          vatom[vlist[m++]][i] += 0.25*v[i]*alpham;
 *        }
 *    }
 */
  }
} 
/* ----------------------------------------------------------------------
  compute position xM of fictitious charge site
------------------------------------------------------------------------- */

void FixPolar::compute_newsite(double *xO,  double *xH1,
                                        double *xH2, double xM[3])
{
  double delx1 = xH1[0] - xO[0];
  double dely1 = xH1[1] - xO[1];
  double delz1 = xH1[2] - xO[2];
  domain->minimum_image(delx1,dely1,delz1);

  double delx2 = xH2[0] - xO[0];
  double dely2 = xH2[1] - xO[1];
  double delz2 = xH2[2] - xO[2];
  domain->minimum_image(delx2,dely2,delz2);

  xM[0] = xO[0] + alpham * 0.5 * (delx1 + delx2);
  xM[1] = xO[1] + alpham * 0.5 * (dely1 + dely2);
  xM[2] = xO[2] + alpham * 0.5 * (delz1 + delz2);
}

/* ----------------------------------------------------------------------
   adjust coordinate of Oxygen atom for TIP4P model 
   ------------------------------------------------------------------------- */
void FixPolar::adjust_oxygen(int iatom, double coor[3])
{
  tagint *tag = atom->tag;
  double **x = atom->x;
  int *type = atom->type;

  int iH1 = atom->map(tag[iatom]+1);
  int iH2 = atom->map(tag[iatom]+2);
  if (iH1 == -1 || iH2 == -1)
    error->one(FLERR,"TIP4P hydrogen is missing for polarization");
  if (type[iH1] != typeH || type[iH2] != typeH)
    error->one(FLERR,"TIP4P hydrogen has incorrect atom type for polarization");
  compute_newsite(x[iatom],x[iH1],x[iH2],coor);
}

/* ----------------------------------------------------------------------
   Function for vector
------------------------------------------------------------------------- */
double FixPolar::innerproduct(double *a, double *b)
{
  double w = 0.0;
  for (int i = 0; i < 3; i++)
    w += (*(a+i))*(*(b+i));
  return w;
}

double FixPolar::innerproduct(double *a, double *b, double frac)
{
  double w = 0.0;
  for (int i = 0; i < 3; i++)
    w += (*(a+i))*(*(b+i));
  return w*frac;
}

void FixPolar::addvec(double *a, double *b, double frac)
{
  //make a += b*frac
  for (int i = 0; i < 3; i++) {
    *(a+i) += frac*(*(b+i));
  }
}
