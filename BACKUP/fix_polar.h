/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under 
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifdef FIX_CLASS

FixStyle(polar,FixPolar)

#else

#ifndef LMP_FIX_POLAR_H
#define LMP_FIX_POLAR_H

#include "fix.h"

namespace LAMMPS_NS {

class FixPolar : public Fix {
 public:
  FixEfield(class LAMMPS *, int, char **);
  ~FixEfield();
  int setmask();
  void init();
  void setup(int);
  void min_setup(int);
  void post_force(int);
  void post_force_respa(int, int, int);
  void min_post_force(int);
  double memory_usage();
  double compute_scalar();
  double compute_vector(int);

 private:
  double ex,ey,ez;
  int varflag,iregion;
  char *xstr,*ystr,*zstr,*estr;
  char *idregion;
  int xvar,yvar,zvar,evar,xstyle,ystyle,zstyle,estyle;
  int nlevels_respa;
  int qflag,muflag;

  double **efield;



  double converter       //unit transfer: kq/r^2->E  mu->E
  
  // external efield
  double efield[3];                     //constant external efield
  char *efield_str[3];                  //efield by variable
  int estyle[3],efield_var[3];          //efield style
  int exeflag;                          //external efield flag


  int force_flag,varflag;
  double fsum[4],fsum_all[4];
 
  // polarization parameter
  double **sefield;                     //constant efield on polarized site
  double **sefield_pol;                 //efield due to polarization
  double *alpha;                        //polarizability in Angstrom^3
  int *polflag;                         //polar flag for atomstyle
  int *apolflag;                        //polflag for atom, =1 if in the group and polarizable
  double T[3][3];

  // input parameter
  double tolerence;                     //calculation tolerence
  int tholeflag;                        //flag for thole's method
  double tholecoff;                     //thole's method coefficient
  double cutoff;                        //Coulumb interaction cutoff
  double mustep;                        //timestep for Lagrangian solver
  int maxiter;                          //maxiteration 

  // Lagrangian solver variable
  double **polmu,**vdt,**acc;           //induced dipole, vdt and acceleration (velocity verlet)
  double mudamp.mustep,mumass;          
  double eneg[3], enegtot[3];
  int packflag;

  //neighbor list
  class NeighList *list;

  //Functions
  void (*thole)(double *, double, int, int)
  void thole_exp(double *, double, int, int)
  void thole_linear(double *, double, int, int)
  void thole_no(double *, double, int, int)

  void cal_sefield_q();
  void cal_sefield_ex();
  void calculate_sefield();
  void compute_T(double **, int, int);
};

}

#endif
#endif

/* ERROR/WARNING messages:

E: Illegal ... command

Self-explanatory.  Check the input script syntax and compare to the
documentation for the command.  You can use -echo screen as a
command-line option when running LAMMPS to see the offending line.

E: Region ID for fix efield does not exist

Self-explanatory.

E: Fix efield requires atom attribute q or mu

The atom style defined does not have this attribute.

E: Variable name for fix efield does not exist

Self-explanatory.

E: Variable for fix efield is invalid style

The variable must be an equal- or atom-style variable.

E: Region ID for fix aveforce does not exist

Self-explanatory.

E: Fix efield with dipoles cannot use atom-style variables

This option is not supported.

W: The minimizer does not re-orient dipoles when using fix efield

This means that only the atom coordinates will be minimized,
not the orientation of the dipoles.

E: Cannot use variable energy with constant efield in fix efield

LAMMPS computes the energy itself when the E-field is constant.

E: Must use variable energy with fix efield

You must define an energy when performing a minimization with a
variable E-field.

*/
