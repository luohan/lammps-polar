
/* ----------------------------------------------------------------------
   Jacobbi iteration, nlocal is the cut of x. 
   nlocal = # of unknowen to be solved
   n      = size of A
   ------------------------------------------------------------------------- */
double FixPolar::jacsolver_iter(matrix_sol Asol)
{
    double res = 0.00;
    int n = Asol.size;
    for (int i = 0; i < n; i++) {
      double xnew = *Asol.b[i];

      for (int j = 0; j < n; j++) 
        xnew -= (*(A[j]+i*n))*(*x[j]);

      xnew += (*(A[i]+i*n))*(*x[i]); //add itself back

      xnew /= *(A[i]+i*n);

      xnew  = xnew*SOR + *x[i]*(1.0-SOR);  
      if ( fabs(xnew - *x[i]) > res)
        res=fabs(xnew - *x[i]);

      *x[i] = xnew;
    }

}
/* ----------------------------------------------------------------------
   Jacobbi  
------------------------------------------------------------------------- */
  <`0`>
void FixPolar::jacsolver(double *Ain, double *bin, double *xin, int n)
{
  double *A[n];   // A is n pointers point to first element of  the row of Ain
  double *b[n];
  double *x[n];
  
  for (int i = 0; i < n; i++){
    A[i] = Ain+i;
    b[i] = bin+i;
    x[i] = xin+i;
  }
  // Equilibrium, make largest column element be one
  if (EQUILFLAG)
    for (int i = 0; i < n; i++) { //loop over rows
      double largest = *(A[0]+i*n);
      for (int j = 1; j < n; j++)   //loop over columns
        if (fabs(*(A[j]+i*n)) > fabs(largest))  largest = *(A[j]+i*n);
      for (int j = 0; j < n; j++){
        *(A[j]+i*n) /= largest;
      }
      *b[i] /= largest;
    }

  // column pivoting 
  if (PIVOTFLAG)
    for (int i = 0; i < n-1; i++) {
      double largest = *(A[i]+i*n);
      int maxcol = i;
      for (int j = i+1; j < n ; j++) {
        if (fabs(*(A[j]+i*n)) > fabs(largest)) {
          largest = *(A[j]+i*n);
          maxcol = j;
        }
      }
      if ( maxcol != i){
        double *ptr = A[i];
        A[i] = A[maxcol];
        A[maxcol] = ptr;

        ptr = x[i];
        x[i] = x[maxcol];
        x[maxcol] = ptr;
      }
    }

  // downward gauss elimination ans swap of column to make diagonal element biggest
  // This is very important to ensure converging.
  if (MAXDIA)
    for (int i = 0; i < n-1; i++) {
      double largest = *(A[i]+i*n);
      int maxcol = i;
      for (int j = i+1; j < n ; j++) {
        if (fabs(*(A[j]+i*n)) > fabs(largest)) {
          largest = *(A[j]+i*n);
          maxcol = j;
        }
      }
      if ( maxcol != i){
        double *ptr = A[i];
        A[i] = A[maxcol];
        A[maxcol] = ptr;

        ptr = x[i];
        x[i] = x[maxcol];
        x[maxcol] = ptr;
      }
      for (int j = i+1; j < n; j++) {
        double fac = (*(A[i]+j*n)) / (*(A[i]+i*n));
        *(A[i]+j*n) = 0.0;
        *b[j] -= *b[i]*fac;
        for (int k = i+1; k < n; k++) {
          *(A[k]+j*n) -= *(A[k]+i*n)*fac;
        }
      }
    }
  
  int iter = 0;
  while ( iter <= maxiter ) {
    double res = 0.00;
    for (int i = 0; i < n; i++) {
      double xnew = *b[i];

      for (int j = 0; j < n; j++) 
        xnew -= (*(A[j]+i*n))*(*x[j]);

      xnew += (*(A[i]+i*n))*(*x[i]); //add itself back

      xnew /= *(A[i]+i*n);

      xnew  = xnew*SOR + *x[i]*(1.0-SOR);  
      if ( fabs(xnew - *x[i]) > res)
        res=fabs(xnew - *x[i]);

      *x[i] = xnew;
    }
    if ( res < tolerance) break;
  }
  if ( iter == maxiter) 
    error->all(FLERR,"Fix_polar doesn't converge, increase maxiter if possible");
}

