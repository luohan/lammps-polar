function out = cosspaceave( data ,field)
%% calculate <cos^2(beta)>
% data = structure of data
% filed is char of field name
% length of data is the number of time steps
% use data.field to calculate the averge of cos^2(beta)
% the angle is in degree
% output is an array

l=length(data);
out = zeros(1,l);
for i=1:l
    out(i)=mean(cosd(data(i).(field)).^2);
end
end

