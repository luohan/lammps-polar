function outputdata = importdata(para)
%% Import LAMMPS Data to a struct
% para.filename : file name
% para.dt       : timestep
% para.fieldname: filed name
% para.unittime : unit of time
% outputdata.data=s             : structure contains the data
% outputdata.time=time          : time of data
% outputdata.size=i             : # of time steps
filename  = para.filename; 
dt        = para.dt;  
fieldname = para.fieldname;
unittime  = para.unittime;
col       = length(fieldname); 
dataform  ='%*d';
for i=1:col
    dataform=strcat(dataform,' %f');
end
dataform=strcat(dataform,'\n');

switch unittime
    case 'ps'
        unittime=1;
    case 'fs'
        unittime=1e-3;
    case 'ns'
        unittime=1e-6;
    case 's'
        unittime=1e-15;
    otherwise
        error('wrong time unit');
end
%% Open file and define variable
fid=fopen(filename,'r');
fgetl(fid);  %read title 
fgetl(fid);  %read title 
fgetl(fid);  %read title 

%% Start Reading
i=0;             %time step counter
while ( feof(fid) ~=1)
    i=i+1;
    title=fscanf(fid,'%d %d\n',2);  %[time step, number of molecule]
    currenttstep=title(1)*unittime;
    nmol=title(2);
    time(i)=currenttstep*dt;        %set time array
    
    data=zeros(nmol,col);
    for j=1:nmol
        row=fscanf(fid,dataform,col);
        row = row';
        data(j,:)=row;
    end
    for j=1:col
        field=fieldname(j);
        s(i).(char(field))=data(:,j);
    end
end
fclose(fid);
outputdata.data=s;          
outputdata.time=time;    
outputdata.size=i ;
end