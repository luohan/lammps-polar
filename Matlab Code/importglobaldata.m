function outputdata = importglobaldata(para)
%% Import LAMMPS Data to a struct
% para.filename : file name
% para.dt       : timestep
% para.fieldname: filed name
% para.unittime : unit of time
% outputdata.data=s             : structure contains the data
% outputdata.time=time          : time of data
% outputdata.size=i             : # of time steps
filename  = para.filename; 
dt        = para.dt;  
fieldname = para.fieldname;
unittime  = para.unittime;
col       = length(fieldname); 
dataform  ='%d';
for i=1:col
    dataform=strcat(dataform,' %f');
end
dataform=strcat(dataform,'\n');

switch unittime
    case 'ps'
        unittime=1;
    case 'fs'
        unittime=1e-3;
    case 'ns'
        unittime=1e-6;
    case 's'
        unittime=1e-15;
    otherwise
        error('wrong time unit');
end
%% Open file and define variable
fid=fopen(filename,'r');
fgetl(fid);  %read title 
fgetl(fid);  %read title 

%% Start Reading
i=1;             %time step counter
data=fscanf(fid,dataform,col+1);
data=data';
while ( feof(fid) ~=1)
    i=i+1;
    line=fscanf(fid,dataform,col+1);
    line=line';
    data=[data;line];
end
outputdata.size=i ;
outputdata.time=data(:,1)*dt*unittime;
for i=1:col
    outputdata.(char(fieldname(i)))=data(:,i+1);
end
fclose(fid);  
end