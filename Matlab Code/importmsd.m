clear
%% import data with E=sin(0.2pi*t), w=0.2e15*pi, f=1e14hz, lamda=3000nm field
para.filename='wat.msd';%data file name
para.dt    = 0.25;             %time step (in fs)
para.unittime = 'fs';
para.fieldname = {'msd'};
data_e=importglobaldata(para);

%% import data with E field
para.filename='wat.2v.msd';%data file name
para.dt    = 0.25;             %time step (in fs)
para.unittime = 'fs';
para.fieldname = {'msd'};
data_2e=importdata(para);

%% import data without E field
para.filename='wat.ne.msd';%data file name
para.dt    = 0.25;             %time step (in fs)
para.unittime = 'fs';
para.fieldname = {'msd'};
data_ne=importdata(para);