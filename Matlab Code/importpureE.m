clear
%% import data with E=sin(0.2pi*t), w=0.2e15*pi, f=1e14hz, lamda=3000nm field
para.filename='wat.euler.time';%data file name
para.dt    = 0.25;             %time step (in fs)
para.unittime = 'fs';
para.fieldname = {'x','y','z','alpha','beta','gamma'};
data_e=importdata(para);

%% import data with E field
para.filename='wat.euler.time2v';%data file name
para.dt    = 0.25;             %time step (in fs)
para.unittime = 'fs';
para.fieldname = {'x','y','z','alpha','beta','gamma'};
data_2e=importdata(para);

%% import data without E field
para.filename='wat.euler.time.noefield';%data file name
para.dt    = 0.25;             %time step (in fs)
para.unittime = 'fs';
para.fieldname = {'x','y','z','alpha','beta','gamma'};
data_ne=importdata(para);

%% calculate <cos^2(\chi)>
data_e.cos=cosspaceave(data_e.data,'beta');
data_ne.cos=cosspaceave(data_ne.data,'beta');
data_2e.cos=cosspaceave(data_2e.data,'beta');

data_e.cosave=ones(1,data_e.size)*mean(data_e.cos);
data_ne.cosave=ones(1,data_ne.size)*mean(data_ne.cos);
data_2e.cosave=ones(1,data_2e.size)*mean(data_2e.cos);


%% output to tecplot format for ave(cos beta)^2
tdata=[];
tdata.title='cos<sup>2</sup>(<greek>b</greek>)';
tdata.Nvar = 2;
tdata.varnames={'time(fs)','<cos<sup>2</sup>(<greek>b</greek>)>'};
tdata.lines=struct('x',{data_e.time,data_e.time,data_2e.time,data_2e.time,data_ne.time,data_ne.time,data_ne.time},...
    'y',{data_e.cos,data_e.cosave,data_2e.cos,data_2e.cosave,data_ne.cos,data_ne.cosave,ones(1,data_ne.size)*(1/3)},...
    'zonename',{'PureE 1V','PureE 1V ave','PureE 2V','PureE 2V ave','No E','No E ave','ISO'});
 mat2tecplot(tdata,'pureE.plt');

%% plot distribution of beta
w=cat(1,data_e.data.beta);
[data_e.dist.x,data_e.dist.y]=plotdf(w,90);
w=cat(1,data_ne.data.beta);
[data_ne.dist.x,data_ne.dist.y]=plotdf(w,90);
w=cat(1,data_2e.data.beta);
[data_2e.dist.x,data_2e.dist.y]=plotdf(w,90);
iso.x=0:1:180;
iso.y=pi/180*sind(iso.x)/2;

tdata=[];
tdata.title='<greek>b</greek> d.f.';
tdata.Nvar = 2;
tdata.varnames={'<greek>b</greek>','d.f.'};
tdata.lines=struct('x',{data_e.dist.x,data_2e.dist.x,data_ne.dist.x,iso.x},'y',...
    {data_e.dist.y,data_2e.dist.y,data_ne.dist.y,iso.y},'zonename',...
    {'PureE 1V','PureE 2V','No E','ISO'});
 mat2tecplot(tdata,'pureEdist.plt');

 
 %% fft signal analysis
 numofmol=1;
 tra=zeros(1,data_e.size);
 for i=1:data_e.size
     tra(i)=data_e.data(i).beta(numofmol);
 end
data_e.fft=mdfft(tra,data_e.size,0.25);
 
 numofmol=1;
 tra=zeros(1,data_2e.size);
 for i=1:data_2e.size
     tra(i)=data_2e.data(i).beta(numofmol);
 end
data_2e.fft=mdfft(tra,data_e.size,0.25);
 
numofmol=1;
 tra=zeros(1,data_ne.size);
 for i=1:data_ne.size
     tra(i)=data_ne.data(i).beta(numofmol);
 end
data_ne.fft=mdfft(tra,data_ne.size,0.25);
 
 
 
 

tdata=[];
tdata.title='FFT Analysis';
tdata.Nvar = 2;
tdata.varnames={'Frequency(Hz)',' '};
tdata.lines=struct('x',{data_e.fft.x,data_2e.fft.x,data_ne.fft.x},'y',...
    {data_e.fft.y,data_2e.fft.y,data_ne.fft.y},'zonename',...
    {'PureE 1V','PureE 2V','No E'});
 mat2tecplot(tdata,'pureEfft.plt');
    
    

