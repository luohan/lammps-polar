clear
clc
mumass = 0.016;
mustep = 0.02;
sefield =linspace(1,20,10);
mudamp=0.1;

alpha =rand(10,10)*2;
while (det(1./alpha) <1)
    alpha=rand(10,10)*2;
end
alpha=1./alpha;

polmu = ones(1,10);
vdt =zeros(1,10);
acc =zeros(1,10);

num =10;
x= zeros(10,num);

for i=1:num
    acc = sefield;
    for j=1:10
        for k=1:10
            acc(j) = acc(j) - polmu(k)*alpha(j,k);
        end
        vdt(j) =vdt(j) + acc(j)/mumass*0.5*mustep^2-mudamp*vdt(j);
        polmu(j) = polmu(j)+vdt(j);
        
        
        for k=1:10
            acc(j) = acc(j) - polmu(k)*alpha(j,k);
        end
%         
%                         tot=sum(acc)/10;
%                         acc =acc-tot;
        vdt(j) =vdt(j) + acc(j)/mumass*0.5*mustep^2-mudamp*vdt(j);
        x(j,i)=polmu(j);
    end
    
end

res=sefield;

for j=1:10
    
    for k=1:10
        res(j) = res(j) - polmu(k)/alpha(j,k);
    end
end



for i=1:10
    figure(2)
    plot(x(i,:))
    hold on
end