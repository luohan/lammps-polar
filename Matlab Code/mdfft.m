function fftb=mdfft( data,size,dt )
 samplefq=1/(dt*1e-15);
 NFFT=2^nextpow2(size)*16;
 f=samplefq/2*linspace(0,1,NFFT/2+1);
 w=data;

fftb.x=f;
k=fft(w',NFFT);
fftb.y=abs(k(1:NFFT/2+1))/size*2;
 
end

