clear
%% import data with E=sin(0.2pi*t), w=0.2e15*pi, f=1e14hz, lamda=3000nm field
para.filename='1v.rdf';%data file name
para.dt    = 0.25;             %time step (in fs)
para.unittime = 'fs';
para.fieldname = {'r','g','num'};
data_e=importdata(para);

%% import data with E field
para.filename='2v.rdf';%data file name
para.dt    = 0.25;             %time step (in fs)
para.unittime = 'fs';
para.fieldname = {'r','g','num'};
data_2e=importdata(para);

%% import data without E field
para.filename='ne.rdf';%data file name
para.dt    = 0.25;             %time step (in fs)
para.unittime = 'fs';
para.fieldname = {'r','g','num'};
data_ne=importdata(para);

tdata=[];
tdata.title='RDF';
tdata.Nvar = 2;
tdata.varnames={'r(A)','g(r<sub>OO</sub>)'};
tdata.lines=struct('x',{data_e.data.r,data_2e.data.r,data_ne.data.r},...
    'y',{data_e.data.g,data_2e.data.g,data_ne.data.g},...
    'zonename',{'PureE 1V','PureE 2V','No E'});
 mat2tecplot(tdata,'pureErdf.plt');
