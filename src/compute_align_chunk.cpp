/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include "string.h"
#include "compute_align_chunk.h"
#include "atom.h"
#include "update.h"
#include "modify.h"
#include "compute_chunk_atom.h"
#include "domain.h"
#include "memory.h"
#include "error.h"
#include <cmath>
#include <cstring>
#include <cstdlib>

using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

ComputeAlignChunk::ComputeAlignChunk(LAMMPS *lmp, int narg, char **arg) : 
  Compute(lmp, narg, arg)
{
  if (narg != 6) error->all(FLERR,"Illegal compute align/chunk command");

  array_flag = 1;
  size_array_cols = 6;  //direction vector and euler angle
  size_array_rows = 0;
  size_array_rows_variable = 1;
  extarray = 0;

  // ID of compute chunk/atom

  int n = strlen(arg[3]) + 1;
  idchunk = new char[n];
  strcpy(idchunk,arg[3]);

  zatom = std::atoi(arg[4]);   // orientation atom, Oxygen here for H2O
  xatom = std::atoi(arg[5]) ;   // 'horizontal' atom type, Hydrogen here for H2O
  
  init();

  // chunk-based data

  nchunk = 1;
  maxchunk = 0;
  massproc = masstotal = NULL;
  com = comall = NULL;

  hprocs  = NULL;
  hprocstemp = NULL;

  vec = NULL;
  euler = NULL;
  vectemp = NULL;
  eulertemp = NULL;
  veceuler = NULL;

  allocate();
}

/* ---------------------------------------------------------------------- */

ComputeAlignChunk::~ComputeAlignChunk()
{
  delete [] idchunk;
  memory->destroy(massproc);
  memory->destroy(masstotal);
  memory->destroy(com);
  memory->destroy(comall);
  
  memory->destroy(hprocs);
  memory->destroy(hprocstemp);
  
  memory->destroy(vec);
  memory->destroy(euler);
  memory->destroy(vectemp);
  memory->destroy(eulertemp);
  memory->destroy(veceuler);
}

/* ---------------------------------------------------------------------- */

void ComputeAlignChunk::init()
{
  int icompute = modify->find_compute(idchunk);
  if (icompute < 0)
    error->all(FLERR,"Chunk/atom compute does not exist for "
               "compute align/chunk");
  cchunk = (ComputeChunkAtom *) modify->compute[icompute];
  if (strcmp(cchunk->style,"chunk/atom") != 0)
    error->all(FLERR,"Compute align/chunk does not use chunk/atom compute");
}

/* ---------------------------------------------------------------------- */

void ComputeAlignChunk::compute_array()
{
  int i,j,index;
  double dx,dy,dz,massone;
  double unwrap[3];

  invoked_array = update->ntimestep;

  // compute chunk/atom assigns atoms to chunk IDs
  // extract ichunk index vector from compute
  // ichunk = 1 to Nchunk for included atoms, 0 for excluded atoms

  nchunk = cchunk->setup_chunks();
  cchunk->compute_ichunk();
  int *ichunk = cchunk->ichunk;
  

  if (nchunk > maxchunk) allocate();
  size_array_rows = nchunk;

  // zero local per-chunk values

  for (int i = 0; i < nchunk; i++) {
    massproc[i] = 0.0;
    hprocstemp[i] = 0;
    for (j = 0; j < 3; j++){
      com[i][j]=0.0;
      vectemp[i][j]=0.0;
      eulertemp[i][j]=0.0;
    }
  }
  // compute COM for each chunk

  double **x = atom->x;
  int *mask = atom->mask;
  int *type = atom->type;
  imageint *image = atom->image;
  double *mass = atom->mass;
  double *rmass = atom->rmass;
  int nlocal = atom->nlocal;

  for (int i = 0; i < nlocal; i++)
    if (mask[i] & groupbit) {
      index = ichunk[i]-1;
      if (index < 0) continue;
      if (rmass) massone = rmass[i];
      else massone = mass[type[i]];
      domain->unmap(x[i],image[i],unwrap);
      massproc[index] += massone;
      com[index][0] += unwrap[0] * massone;
      com[index][1] += unwrap[1] * massone;
      com[index][2] += unwrap[2] * massone;
    }

  MPI_Allreduce(massproc,masstotal,nchunk,MPI_DOUBLE,MPI_SUM,world);
  MPI_Allreduce(&com[0][0],&comall[0][0],3*nchunk,MPI_DOUBLE,MPI_SUM,world);

  for (int i = 0; i < nchunk; i++) {
    comall[i][0] /= masstotal[i]; 
    comall[i][1] /= masstotal[i];
    comall[i][2] /= masstotal[i];
  }
  
  int myid;
  MPI_Comm_rank(world,&myid);
  //compute orientation vector
  for (int i = 0; i < nlocal; i++)
    if (mask[i] & groupbit){
      index = ichunk[i]-1;
      if (index < 0) continue;
      if (type[i] == zatom){
        domain->unmap(x[i],image[i],unwrap);
        vectemp[index][0] = unwrap[0]-comall[index][0];
        vectemp[index][1] = unwrap[1]-comall[index][1];
        vectemp[index][2] = unwrap[2]-comall[index][2];
        vecnomal(*(vectemp+index));   //nomalize the vector
      }
      else if (type[i] == xatom){
        hprocstemp[index]=myid;
      }
    }
  MPI_Allreduce(hprocstemp,hprocs,nchunk,MPI_INT,MPI_MAX,world);   
  //use the hydrogen in largest # procs to calcultae

  MPI_Allreduce(&vectemp[0][0],&vec[0][0],3*nchunk,
                 MPI_DOUBLE,MPI_SUM,world);  //orientation vector is only nonzero for one process

  for (int i=0; i < nlocal; i++)
    if (mask[i] & groupbit){
      index = ichunk[i]-1;
      if ( index >=  0 )
        if ( myid == hprocs[index])
          if (type[i] == xatom){
            domain->unmap(x[i],image[i],unwrap);

            double hvector[3];            
            double pi=3.1415926;
            double zvec[3]={0,0,1};
            double *r, *ypvec;

            double zpvec[3];                                       //vector com--O 
            
            hvector[0] = unwrap[0]-comall[index][0];               //vector com--H
            hvector[1] = unwrap[1]-comall[index][1];
            hvector[2] = unwrap[2]-comall[index][2];
            vecnomal(hvector);

            for (int j=0; j<3 ; j++)  zpvec[j]=vec[index][j]; // z'axis
            vecnomal(zpvec);

            eulertemp[index][1] = std::acos(zpvec[2])/pi*180;                      //euler angle beta
            
            r = vecper(zpvec,hvector);                   //x' axis
            if (r[0] <0) for( int j=0;j<3;j++) *(r+j)=-*(r+j);              //ensure euler angle gamma<90d
            vecnomal(r);

            ypvec=new double[3];
            ypvec[0] = zpvec[1]*r[2]-zpvec[2]*r[1];      //y' axis
            ypvec[1] =-zpvec[0]*r[2]+zpvec[2]*r[0];
            ypvec[2] = zpvec[0]*r[1]-zpvec[1]*r[0];
            vecnomal(ypvec);

            eulertemp[index][0] = atan2(zpvec[0],-zpvec[1])/pi*180; //euler angle alpha
            eulertemp[index][2] = atan2(r[2],ypvec[2])/pi*180;      //euler angle gamma

            delete[] r;
            delete[] ypvec;
          }
    }

  MPI_Allreduce(&eulertemp[0][0],&euler[0][0],3*nchunk,
               MPI_DOUBLE,MPI_SUM,world);   //each chunk's euler angle is only nonzero for one process

  for (int i=0;i<nchunk;i++)
    for (int j=0;j<3;j++){
      veceuler[i][j]   = vec[i][j];
      veceuler[i][j+3] = euler[i][j];
    }
}

/* ----------------------------------------------------------------------
   lock methods: called by fix ave/time
   these methods insure vector/array size is locked for Nfreq epoch
     by passing lock info along to compute chunk/atom
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   increment lock counter
------------------------------------------------------------------------- */

void ComputeAlignChunk::lock_enable()
{
  cchunk->lockcount++;
}

/* ----------------------------------------------------------------------
   decrement lock counter in compute chunk/atom, it if still exists
------------------------------------------------------------------------- */

void ComputeAlignChunk::lock_disable()
{
  int icompute = modify->find_compute(idchunk);
  if (icompute >= 0) {
    cchunk = (ComputeChunkAtom *) modify->compute[icompute];
    cchunk->lockcount--;
  }
}

/* ----------------------------------------------------------------------
   calculate and return # of chunks = length of vector/array
------------------------------------------------------------------------- */

int ComputeAlignChunk::lock_length()
{
  nchunk = cchunk->setup_chunks();
  return nchunk;
}

/* ----------------------------------------------------------------------
   set the lock from startstep to stopstep
------------------------------------------------------------------------- */

void ComputeAlignChunk::lock(Fix *fixptr, bigint startstep, bigint stopstep)
{
  cchunk->lock(fixptr,startstep,stopstep);
}

/* ----------------------------------------------------------------------
   unset the lock
------------------------------------------------------------------------- */

void ComputeAlignChunk::unlock(Fix *fixptr)
{
  cchunk->unlock(fixptr);
}


/* ----------------------------------------------------------------------
   free and reallocate per-chunk arrays
------------------------------------------------------------------------- */

void ComputeAlignChunk::allocate()
{
  memory->destroy(massproc);
  memory->destroy(masstotal);
  memory->destroy(com);
  memory->destroy(comall);
 
  memory->destroy(hprocs);
  memory->destroy(hprocstemp);

  memory->destroy(veceuler);
  memory->destroy(vec);
  memory->destroy(euler);
  memory->destroy(vectemp);
  memory->destroy(eulertemp);

  maxchunk = nchunk;
  memory->create(massproc,maxchunk,"align/chunk:massproc");
  memory->create(masstotal,maxchunk,"align/chunk:masstotal");
  memory->create(com,maxchunk,3,"align/chunk:com");
  memory->create(comall,maxchunk,3,"align/chunk:comall");
  
  memory->create(veceuler,maxchunk,6,"align/chunk:veceuler");
  memory->create(vec,maxchunk,3,"align/chunk:vec");
  memory->create(vectemp,maxchunk,3,"align/chunk:vectemp");
  memory->create(euler,maxchunk,3,"align/chunk:euler");
  memory->create(eulertemp,maxchunk,3,"align/chunk:eulertemp");

  memory->create(hprocs,maxchunk,"align/chunk:hprocs");
  memory->create(hprocstemp,maxchunk,"align/chunk:hprocstemp");
  array = veceuler;
}

/* ----------------------------------------------------------------------
   memory usage of local data
------------------------------------------------------------------------- */

double ComputeAlignChunk::memory_usage()
{
  double bytes = (bigint) maxchunk * 2 * sizeof(double);
  bytes += (bigint) maxchunk * 2*3 * sizeof(double);
  bytes += (bigint) maxchunk * 2*6 * sizeof(double);
  return bytes;
}

double *ComputeAlignChunk::vecper(double a[3], double b[3])
{
  //return a vector in the same plane as vector a and b
  //returned vector is perpendicular to vector a
  //if i = 0; returned vector = a+fac*b
  //if i = 1; returned vector = a+w*b
  double *r = new double[3];
  double d1=0.0, d2=0.0,fac=0.0;
  for (int j=0;j<3;j++){
    d1+=a[j]*a[j];
    d2+=a[j]*b[j];
  }
  if (d2 ==0.0)
  {
    delete[] r;
    r = b;
  }else{
    fac = -d1/d2;
    for (int j=0;j<3;j++) r[j]=fac*b[j]+a[j];
  }
  return r;
}

void ComputeAlignChunk::vecnomal(double *a)
{
  // nomalize vector a
  double d1=0.0;
  for (int i=0;i<3;i++) d1+=(*(a+i))*(*(a+i));
  d1=std::sqrt(d1);
  for (int i=0;i<3;i++) *(a+i)/=d1;
}
